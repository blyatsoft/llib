#
#
# LLib command line options
#
#

#
# Compile options
#
option(USE_AVX 				"Enable the use of AVX stuff in the engine." ON)
option(USE_SSE3 			"Enable the use of SSE3 stuff in the engine." ON)
option(USE_AVX_VEC 			"Enable vectorization for AVX. Note: This will cause the engine to not be runnable on older machines" OFF)
option(USE_SSE3_VEC 		"Enable vectorization for SSE3" ON)
option(FAST_MATH 			"Enable faster math where possible." ON)
option(NO_SSE 				"Forces the engine to not use any SSE or AVX optimizations" OFF)
option(UNIX_CROSS 			"Generate projects for windows while on UNIX. Use with MinGW makefiles." OFF)
option(BUILD_64BIT 			"Build for 64-bit platforms" ON)
option(USE_RDRAND_ASM		"Use hardware random number generation." OFF)
option(USE_HARDWARE_CRYPTO 	"Use hardware crypto functions." ON)
option(USE_CXX_20 			"Force the use of std c++20" OFF)

# Basic compile defs
if(BUILD_64BIT)
	add_definitions(-DPLATFORM_64BITS -D_X64_ -D_x64_ -DPLATFORM_64BIT)
else()
	add_definitions(-DPLATFORM_32BITS -D_X86_ -D_I386_ -D_i386_ -D_x86_ -DPLATFORM_32BIT)
endif(BUILD_64BIT)

# Setup stuff for unix
if(DEFINED CMAKE_HOST_UNIX AND NOT UNIX_CROSS)
	add_definitions(-DPOSIX -D_POSIX -DDLL_EXT=.so)
	set(POSIX 1)
	if(BUILD_64BIT)
		set(POSIX64 1)
		add_definitions(-DLINUX64 -D_LINUX64 -DPOSIX64 -D_POSIX64)
		set(CMAKE_ASM_NASM_OBJECT_FORMAT elf32)
	else()
		set(POSIX32 1)
		add_definitions(-DLINUX32 -D_LINUX32 -DPOSIX32 -D_POSIX32)
		set(CMAKE_ASM_NASM_OBJECT_FORMAT elf)
		
		# Needed to force 32-bit build on 64-bit
		set(CMAKE_CXX_FLAGS 	"${CMAKE_CXX_FLAGS} -m32")
		set(CMAKE_C_FLAGS 		"${CMAKE_C_FLAGS} -m32")
		set(CMAKE_LINK_FLAGS 	"${CMAKE_LINK_FLAGS} -m32")
	endif(BUILD_64BIT)
endif(DEFINED CMAKE_HOST_UNIX AND NOT UNIX_CROSS)

# We can build for windows on unix if we would like
if(DEFINED CMAKE_HOST_WIN32 OR UNIX_CROSS)
	add_definitions(-DWIN32 -DWINDOWS -D_WINDOWS -DDLL_EXT=.dll)
	set(WINDOWS 1)
	if(BUILD_64BIT)
		set(WINDOWS64 1)
		add_definitions(-DWIN64 -D_WIN64)
		set(CMAKE_ASM_NASM_OBJECT_FORMAT win64)
	else()
		set(WINDOWS32 1)
		add_definitions(-DWIN32 -D_WIN32)
		set(CMAKE_ASM_NASM_OBJECT_FORMAT win32)
	endif(BUILD_64BIT)
endif(DEFINED CMAKE_HOST_WIN32 OR UNIX_CROSS)


if(FAST_MATH)
	set(USE_SIMD 1)
	set(USE_SIMD_VECTORS 1)
	set(USE_SIMD_MATRICES 1)
	set(USE_MATH_ASM 1)
	add_definitions(-D_OPTIMIZED_MATH=1)
endif(FAST_MATH)

if(DEFINED USE_SIMD)
	
	# Win64 does not allow mmx intrinsics, yay.
	if(NOT DEFINED WINDOWS64)
		add_definitions(-D_USE_MMX=1)
	endif(NOT DEFINED WINDOWS64)
	
	add_definitions(-D_USE_SIMD=1)

	if(DEFINED USE_AVX)
		add_definitions(-D_USE_AVX=1)
	endif(DEFINED USE_AVX)

	if(USE_SSE3)
		add_definitions(-D_USE_SSE=1)
		add_definitions(-D_USE_SSE1=1)
		add_definitions(-D_USE_SSE2=1)
		add_definitions(-D_USE_SSE3=1)
	endif(USE_SSE3)

	if(DEFINED USE_FMA)
		add_definitions(-D_USE_FMA=1)
	endif(DEFINED USE_FMA)

	if(DEFINED USE_AVX512)
		add_definitions(-D_USE_AVX512=1)
	endif(DEFINED USE_AVX512)

endif(DEFINED USE_SIMD)

if(DEFINED USE_SIMD_VECTORS)
	add_definitions(-D_SIMD_VECTORS=1)
endif(DEFINED USE_SIMD_VECTORS)

if(DEFINED USE_HARDWARE_CRYPTO)
	add_definitions(-D_HARDWARE_CRYPTO=1)
endif(DEFINED USE_HARDWARE_CRYPTO)

if(DEFINED USE_SIMD_MATRICES)
	add_definitions(-D_SIMD_MATRICES=1)
endif(DEFINED USE_SIMD_MATRICES)

if(USE_RDRAND_ASM)
	add_definitions(-D_RDRAND_ASM)
endif(USE_RDRAND_ASM)

if(DEFINED USE_MATH_ASM)
	add_definitions(-D_MATH_ASM=1)
endif(DEFINED USE_MATH_ASM)

# C++ standard stuff
if(USE_CXX_20)
	set(GLOBAL_CXX_STANDARD		20)
else()
	set(GLOBAL_CXX_STANDARD		17)
endif(USE_CXX_20)
