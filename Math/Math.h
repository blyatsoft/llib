#pragma once

#include "LLib.h"

#ifndef _MATH_ASM
#include <xmmintrin.h>
#endif

//LLIB_API float  m_sqrtf(float f);
LLIB_API double m_sqrt(double d);

LLIB_MATH_API_ASM float		_m_sqrtf	(	float f		);
LLIB_MATH_API_ASM double	_m_sqrt		(	double d	);
LLIB_MATH_API_ASM float		_m_cosf		(	float f		);
LLIB_MATH_API_ASM double	_m_cos		(	double d	);
LLIB_MATH_API_ASM float		_m_sinf		(	float f		);
LLIB_MATH_API_ASM double	_m_sin		(	double d	);
LLIB_MATH_API_ASM float		_m_tanf		(	float f		);
LLIB_MATH_API_ASM double	_m_tan		(	double d	);
LLIB_MATH_API_ASM float		_m_absf		(	float f		);
LLIB_MATH_API_ASM double	_m_abs		(	double d	);
LLIB_MATH_API_ASM float		_m_rsqrtf	(	float f		);

LLIB_HEADER_START

//I like things large
constexpr const double PI = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862;

constexpr const double e = 2.718281828459045235360287471352662497757247093699959574966967627724076630353;

//NaN constants
constexpr const double DoubleNaN = 0x7FF0000000000001;

constexpr const float FloatNaN = 0x7F800001;

FORCEINLINE constexpr double factorial(uint32 x)
{
	if (x == 0)
		return 1;

	double ret = 1;

	for(; x >= 1; x--)
	{
		ret *= x;
	}

	return ret;
}

//Computes PI by n (PI/n1)
FORCEINLINE constexpr double PI_N(double n1)
{
	return LLib::PI / n1;
}

FORCEINLINE constexpr double divide(double n1, double n2)
{
	return n1 / n2;
}


float	m_fastcosf	(float f);
double	m_fastcos	(double d);
float	m_cosf		(float f);
double	m_cos		(double d);
float	m_fastsinf	(float f);
double	m_fastsin	(double d);
float	m_sinf		(float f);
double	m_sin		(double d);
float	m_fasttanf	(float f);
double	m_fasttan	(double d);
float	m_tanf		(float f);
double	m_tan		(double d);



float 	sqrtf(float f);
double 	sqrt(double d);
float 	cosf(float f);
double 	cos(double d);
float 	sinf(float f);
double 	sin(double d);
float 	tanf(float f);
double 	tan(double d);
float 	absf(float f);
double 	abs(double d);
float 	lerpf(float n0, float n1, float bias);
double 	lerp(double n0, double n1, double bias);

LLIB_HEADER_END