
#include "Math.h"
//extern void _m_sqrt(void);

#include <math.h>

LLIB_SOURCEFILE

FORCEINLINE float LLib::sqrtf(float f)
{
#ifdef _MATH_ASM
	return _m_sqrtf(f);
#else
	__m128 param = _mm_load_ps1(&f);

	param = _mm_sqrt_ps(param);

	return *reinterpret_cast<float*>(&param);

#endif //_MATH_ASM
}

FORCEINLINE double LLib::sqrt(double d)
{
#ifdef _MATH_ASM
	return _m_sqrt(d);
#else
	__m128d param = _mm_load1_pd(&d);

	param = _mm_sqrt_pd(param);

	return *reinterpret_cast<float*>(&param);
#endif
}

FORCEINLINE float LLib::cosf(float f)
{
#ifdef _MATH_ASM
	return _m_cosf(f);
#else

#ifdef _FAST_MATH
	return m_fastcosf(f);
#else
	return m_cosf(f);
#endif

#endif
}

FORCEINLINE double LLib::cos(double d)
{
#ifdef _MATH_ASM
	return _m_cos(d);
#else

#ifdef _FAST_MATH
	return m_fastcos(d);
#else
	return m_cos(d);
#endif

#endif
}

FORCEINLINE float LLib::sinf(float f)
{
#ifdef _MATH_ASM
	return _m_sinf(f);
#else

#ifdef _FAST_MATH
	return m_fastsinf(f);
#else
	return m_sinf(f);
#endif

#endif
}

FORCEINLINE double LLib::sin(double d)
{
#ifdef _MATH_ASM
	return _m_sin(d);
#else

#ifdef _FAST_MATH
	return m_fastsin(d);
#else
	return m_sin(d);
#endif

#endif
}

FORCEINLINE float LLib::tanf(float f)
{
#ifdef _MATH_ASM
	return _m_tanf(f);
#else

#ifdef _FAST_MATH
	return m_fasttanf(f);
#else
	return m_tanf(f);
#endif

#endif
}

FORCEINLINE double LLib::tan(double d)
{
#ifdef _MATH_ASM
	return _m_tan(d);
#else

#ifdef _FAST_MATH
	return m_fasttan(d);
#else
	return m_tan(d);
#endif

#endif
}

FORCEINLINE float LLib::absf(float f)
{
#ifdef _MATH_ASM
	return _m_absf(f);
#else

#endif
}

FORCEINLINE double LLib::abs(double d)
{
#ifdef _MATH_ASM
	return _m_abs(d);
#else

#endif
}

FORCEINLINE float LLib::lerpf(float n0, float n1, float bias)
{
	return n0 + bias / 1.0f * (n1 - n0);
}

FORCEINLINE double LLib::lerp(double n0, double n1, double bias)
{
	return n0 + bias / 1.0 * (n1 - n0);
}