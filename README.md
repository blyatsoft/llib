# Lettuce Lib (LLIB)

## Building
Make sure you have the NASM assembler installed on your system and added to the path
Also make sure you have GCC or MSVC installed on your system
The script generateprojects.bat will generate Visual Studio 2017 projects for both x86 and x64 platforms

## Component summary

### Containers/
Containers library, defines the containers listed below

### Crypto/
Croptography library- Implements SHA-1, SHA-2, SHA-3, MD5 hashes and AES encryption

### Platform/
Platform specific implementations of things such as library loading, basic networking, etc.

### IO/
IO Library- Redefines classes used to do read/write operations on files and stuff
Also defines serialization rules.

### Math/
Math library- Reimplements a bunch of stuff in the C/C++ standard math library

### Matrix/
Also a part of the math library- Implements matrix classes and their corresponding serialization rules

### Net/
Networking library- Implements numerous networking utilities like UDP, TCP networking and utils for working with HTTP and HTTPS

### Threading/
Multithreading library- Implements threads and other things as such. Includes atomic operations too!

### Util/
Utilities Library- Implements a bunch of utilities...yay

### Vector/
Vectors library- Implements vector classes and their serialization rules

## Containers Summary
FixedArray - A wrapper around a constant pointer to a normal C-style array. Provides search functions and a number of other things.

Array - A dynamically sized array with a bunch of useful functions.

ConcurrentArray - A thread-safe version of the Array class

Map - A dynamically sized associative container with keys and values

ConcurrentMap - A thread-safe version of Map

KeyValuePair - A pair of values, one is a key and the other is a value

Stack - A dynamically sized FILO structure

Queue - A thread-safe, dynamically sized FIFO structure

PriorityQueue - A thread-safe, dynamically sized FIFO structure with a priority system.

BasicString - A templated string class

String - A simple ASCII encoded string

StringUTF8 - A simple UTF-8 encoded string

StringUTF16 - A simple UTF-16 encoded string

Vector2 - A 2 component vector template

Vector3 - A 3 component vector template

Vector4 - A 4 component vector template