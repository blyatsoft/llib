/*

Pointer.h

LLib Smart pointer library

*/
#pragma once

#include "../LLib.h"

#include <memory>
#include <cstring>

LLIB_HEADER_START

/*

SEMANTICS OVERVIEW



*/

/*

Pointer

A smart pointer class. 
This class will own the pointers you feed it and it will destroy them when it comes out of scope.
It is undefined behaviour if you delete the pointer yourself. In debug mode, an assertion is thrown if you delete the internal pointer.
You cannot reassign the pointer this references.
*/
template<class T>
class Pointer
{
private:
	T * m_ptr = NULL;

public:
	template<class ...Args>
	Pointer(Args... args)
	{
		m_ptr = new T(args...);
	}

	Pointer(T* ptr)
	{
		LLIB_ASSERT(m_ptr != NULL);
		m_ptr = ptr;
	}

	~Pointer()
	{
		LLIB_ASSERT(m_ptr != NULL);
		delete m_ptr;
	}

public:
	FORCEINLINE T* GetPointer() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE T& GetReference() const
	{
		LLIB_ASSERT(m_ptr);
		return *m_ptr;
	}

	FORCEINLINE T* Get() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE void Swap(Pointer<T>& other)
	{
		LLIB_ASSERT(m_ptr);
		LLIB_ASSERT(other.m_ptr);

		T* temp = other.m_ptr;
		other.m_ptr = this>m_ptr;
		this->m_ptr = temp;
	}

	FORCEINLINE T* operator*() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE T* operator->() const
	{
		LLIB_ASSERT(m_ptr);
		return m_ptr;
	}

	FORCEINLINE bool operator==(const T* other) const
	{
		return m_ptr == other;
	}

	FORCEINLINE bool operator==(const Pointer<T>& other) const
	{
		return m_ptr == other.m_ptr;
	}

	FORCEINLINE bool operator!=(const T* other) const
	{
		return m_ptr != other;
	}

	FORCEINLINE bool operator!=(const Pointer<T>& other) const
	{
		return m_ptr != other.m_ptr;
	}

	FORCEINLINE Pointer<T>& operator=(const T* other) const
	{
		Pointer<T> _new(other);
		return _new;
	}
};


template<class T>
class SharedPointer;

/*

SharedReference

A special type of reference that references a SharedPointer

*/
template<class T>
class SharedReference
{
private:
	T& m_ref;
	
	template<class _>
	friend class SharedPointer;
	
	SharedPointer<T>* m_pointer;
	
	SharedReference() = delete;

	SharedReference(T& p, SharedPointer<T>* ptr)
	{
		m_ref = p;
		m_pointer = ptr;
	};
	
public:
	SharedReference(SharedPointer<T>& ptr) :
		m_ref(*ptr.m_ptr)
	{
		m_pointer = &ptr;
		m_pointer->m_refcount++;
	}
	
	~SharedReference()
	{
		if(m_pointer)
			m_pointer->RemoveReference();
	}
	
public:
	
	FORCEINLINE T& Get() const
	{
		return m_ref;
	}
	
	FORCEINLINE T* GetPointer() const
	{
		return &m_ref;
	}
	
public:
	
	FORCEINLINE T& operator*() const
	{
		return m_ref;
	}
	
	FORCEINLINE T* operator&() const
	{
		return &m_ref;
	}
	
	FORCEINLINE bool operator==(const SharedReference<T>& other) const
	{
		return this->m_ref == other.m_ref;
	}
	
	FORCEINLINE bool operator!=(const SharedReference<T>& other) const
	{
		return this->m_ref != other.m_ref;
	}
	
	FORCEINLINE SharedReference<T>& operator=(SharedPointer<T>& other) const
	{
		return other.CreateReference();
	}
	
	FORCEINLINE bool operator>(const SharedReference<T>& other) const
	{
		return this->m_ref > other.m_ref;
	}
	
	FORCEINLINE bool operator<(const SharedReference<T>& other) const
	{
		return this->m_ref < other.m_ref;
	}
	
	FORCEINLINE bool operator>=(const SharedReference<T>& other) const
	{
		return this->m_ref >= other.m_ref;
	}
	
	FORCEINLINE bool operator<=(const SharedReference<T>& other) const
	{
		return this->m_ref <= other.m_ref;
	}
};


/*

SharedPointer

A special type of pointer that is reference counted and referenced by a SharedReference
This pointer cannot be destroyed by the programmer, instead it can only be destroyed implicitly when the last 
SharedReference referencing this pointer is destroyed.

*/
template<class T>
class SharedPointer
{
private:
	T* m_ptr = NULL;
	unsigned m_refcount = 0;
	
	template<class _>
	friend class SharedReference;
	
	~SharedPointer()
	{
		delete m_ptr;
	}
	
	FORCEINLINE void RemoveReference()
	{
		m_refcount--;
		if(m_refcount <= 0)
			delete this;
	}
	
	SharedPointer() = default;
	
public:
	
	template<class ...Args>
	SharedPointer(Args... args)
	{
		m_ptr = new T(args...);
	}
	
	FORCEINLINE SharedReference<T> CreateReference()
	{
		return SharedReference<T>(*this);
	}
	
	FORCEINLINE unsigned GetRefCount() const
	{
		return m_refcount;
	}
	
	// Attempts to destroy this object, if the current refcount == 0
	FORCEINLINE void TryDestroy()
	{
		if(m_refcount == 0)
			delete this;
	}
	
	FORCEINLINE SharedPointer<T>& Duplicate() const
	{
		SharedPointer<T>& ptr = *(new SharedPointer<T>());
		memcpy(&ptr, m_ptr, sizeof(T));
		return ptr;
	}
	
	// Needed to prevent deletion of this by accident, assertion invoked as calling this is an err
	FORCEINLINE void operator delete(void* _this)
	{
		_llib_assert(0);
		SharedPointer<T>& __this = *(SharedPointer<T>*)_this;
		if(__this.m_refcount <= 0)
			free(_this);
	}
};



LLIB_HEADER_END
