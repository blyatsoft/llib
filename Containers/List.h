/*

List.h

List container types

*/
#pragma once

#include "LLib.h"

#include "Allocator2.h"

///////////////////////
//Forward decls
class DefaultAllocator;

///////////////////////

LLIB_HEADER_START

/*
LinkedList<T, I, A>

A doubly linked list type
This isn't an extremely efficient implementation, so it is recommended that you use the Array type instead

*/
template<class T, class I = int>
class LinkedList final /* No children, sorry! */
{
private:

	typedef I index_t;
	typedef I length_t;

	class CListNode
	{
		I prev_node = 0;
		I next_node = 0;
		T value;
		bool free = true;
	};

	I m_count;

	I m_size;

	IAllocator2* m_allocator;

	/*
	m_contents is the base pointer where all elements will be inserted
	m_contents[0] is the first node in the list.
	*/
	CListNode* m_contents;

	FORCEINLINE void InternalResize(length_t size)
	{
		if (m_contents) 
		{
			m_contents = (CListNode*)m_allocator->Reallocate(m_contents, sizeof(CListNode) * size);
			m_size = size;
		}
		else
		{
			m_contents = (CListNode*)m_allocator->Reallocate(sizeof(CListNode) * size);
			m_size = size;
		}
	}

public:
	LinkedList(IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		m_allocator = allocator;

		InternalResize(1);
	}

	LinkedList(length_t length, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		m_allocator = allocator;

		InternalResize(length);
	}

	LinkedList(const LinkedList<T, I>& other)
	{
		m_allocator = other.m_allocator;

		memcpy(m_contents, other.m_contents, m_size);

		m_count = other.m_count;

		m_size = other.m_size;
	}

	LinkedList(const LinkedList<T, I>&& other) noexcept
	{
		m_allocator = other.m_allocator;

		m_contents = other.m_contents;

		m_count = other.m_count;

		m_size = other.m_size;

		other.m_contents = NULL;

		other.m_size = 0;

		other.m_count = 0;
	}

	~LinkedList()
	{
		m_allocator->Free(m_contents);
	}

public:
	void PushBack(const T& elem)
	{
		
	}

	void PushBack(const T&& elem)
	{
		
	}

	void PopBack()
	{
		
	}

};

template<class T, class I = int>
class DoublyLinkedList
{
private:

	typedef I length_t;
	typedef I index_t;
	typedef T* TPtr_t;

	struct DoublyLinkedNode
	{
		TPtr_t m_prev_node;
		TPtr_t m_next_node;

		T m_value;

		DoublyLinkedNode(TPtr_t prev, TPtr_t next, T val)
		{
			m_prev_node = prev;
			m_next_node = next;
			m_value = val;
		}
	};

	IAllocator2* m_allocator;

	DoublyLinkedNode* m_firstelem = NULL;

	T* m_freespaces = NULL;
	I m_freespaces_count = 0;

	I m_count = 0;

	//Return allocator
	FORCEINLINE IAllocator2* InternalGetAllocator() const
	{
		return m_allocator;
	}

	FORCEINLINE T InternalGetEmpty() const
	{
		char arr[sizeof(T)];
		return *(T*)arr;
	}

	FORCEINLINE void InternalPushBack(T value)
	{
		if(m_firstelem == NULL)
			return;

		DoublyLinkedNode* current_node = m_firstelem;
		for(I i = 0; current_node->m_next_node+1 != 0; i++)
		{
			if(current_node->m_next_node == 0)
			{
				if(m_freespaces_count > 0)
				{
					m_freespaces[0] = DoublyLinkedNode(current_node, 0, value);
					current_node->m_next_node = m_freespaces[0];
					m_freespaces = m_freespaces + 1;
					m_freespaces_count--;
					m_count++;
					break;
				}

				DoublyLinkedNode* nnode = new DoublyLinkedNode(current_node, 0, value);
				current_node->m_next_node = nnode;
				m_count++;
				break;
			}
			current_node = current_node->m_next_node;
		}

		return;
	}

	FORCEINLINE void InternalPushFront(T value)
	{
		if(m_firstelem == NULL)
		{
			if(m_freespaces_count > 0)
			{
				m_freespaces[0]  = DoublyLinkedNode(0, 0, value);
				m_firstelem = m_freespaces[0];
				m_freespaces++;
				m_freespaces_count--;
			}
			else
				m_firstelem = new DoublyLinkedNode(0, 0, value);
		}
		else
		{
			if(m_freespaces_count > 0)
			{
				m_freespaces[0] = DoublyLinkedNode(0, m_firstelem, value);
				m_firstelem->m_prev_node = m_freespaces[0];
				m_freespaces_count--;
				m_freespaces++;
				m_count++;
			}
			else
			{
				m_firstelem->m_next_node = new DoublyLinkedNode(0, m_firstelem, value);
				m_count++;
			}
		}
	}

	FORCEINLINE void InternalInsertAfter(T value)
	{

	}

	FORCEINLINE void InternalInsertBefore(T value)
	{

	}

	FORCEINLINE void AllocateSpace(I size)
	{

	}

	FORCEINLINE void InternalReplace(T value)
	{

	}

	FORCEINLINE void InternalInsert(T value, I index)
	{

	}

	FORCEINLINE void LoadFromArray(const T* value, I length)
	{
		if(length <= 0)
			return;

		m_firstelem = DoublyLinkedNode(0, 0, value[0]);
		DoublyLinkedNode* prev_node = m_firstelem;

		for(I i = 1; i < length; i++)
		{
			DoublyLinkedNode* nnode = new DoublyLinkedNode(prev_node, 0, value[i]);
			prev_node->m_next_node = nnode;
			prev_node = nnode;
		}
	}

	FORCEINLINE void Free()
	{
		if(m_firstelem == NULL)
			return;

		DoublyLinkedNode* current_node = m_firstelem;

		do
		{
			DoublyLinkedNode* next = current_node->m_next_node;
			DoublyLinkedNode* prev = current_node->m_prev_node;

			m_allocator->Free(current_node);

			current_node = next;

		} while(current_node != NULL);
	}


public:

	DoublyLinkedList(IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		m_allocator = allocator;
		m_firstelem = (T*)allocator->Allocate(sizeof(T) * 1);
		m_firstelem[0] = DoublyLinkedNode(0, 0, InternalGetEmpty());
		m_count = 0;
	}

	DoublyLinkedList(length_t reserved_space, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		m_allocator = allocator;
		m_firstelem = (T*)allocator->Allocate(sizeof(T) * reserved_space);
		m_allocator->ZeroMemory(m_firstelem, sizeof(T));
		m_count = 0;
	}

	DoublyLinkedList(const DoublyLinkedList<T, I>& other)
	{
		m_allocator = other.InternalGetAllocator();
		m_firstelem = other.m_firstelem;
		m_count = other.m_count;
	}

	DoublyLinkedList(DoublyLinkedList<T, I>&& other)
	{
		m_allocator = other.m_allocator;
		m_firstelem = other.m_firstelem;
		m_count = other.m_count;
		other.m_count = 0;
		other.m_firstelem = NULL;
	}

	DoublyLinkedList(const T* other, length_t len, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{

	}

	DoublyLinkedList(T* other, length_t len, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{

	}

};

LLIB_HEADER_END