/*

Stack.h

Defines stack structures

*/
#pragma once

#include "LLib.h"

#include "Allocator2.h"
#include "Array.h"

#include <exception>
#include <stdexcept>

LLIB_HEADER_START

template<class T, class I>
class Array;

template<class T, class I = int>
class Stack
{
private:
	typedef I index_t;
	typedef I length_t;

	/*
	Contents of the stack
	*/
	T* m_contents = NULL;

	/*
	Top of the stack
	*/
	index_t m_top = 0;

	/*
	Size of the stack
	*/
	length_t m_size = 0;

	/*
	Class allocator
	*/
	IAllocator2* m_allocator;

public:

	Stack(IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		m_contents = (T*)m_allocator->AllocateArray(1, sizeof(T));
	}

	Stack(length_t size, IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		m_contents = (T*)m_allocator->AllocateArray(size, sizeof(T));
	}

	//Copy constructor
	Stack(const Stack<T, I>& other)
	{
		m_allocator = other.m_allocator;
		m_contents = m_allocator->Allocate(sizeof(T) * other.m_size);
		m_allocator->CopyMemory(m_contents, other.m_contents, sizeof(T) * other.m_size);
		m_size = other.m_size;
		m_top = other.m_top;
	}

	Stack(const Stack<T, I>&& other)
	{
		m_allocator = other.m_allocator;
		m_contents = other.m_contents;
		m_size = other.m_size;
		m_top = other.m_top;

		other.m_contents = NULL;
		other.m_top = 0;
		other.m_size = 0;
	}

	Stack(const T* elems, length_t size, IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		m_contents = (T*)m_allocator->Allocate(sizeof(T) * size);

		memcpy(m_contents, elems, size);
	}

	~Stack()
	{
		if(m_contents)
			m_allocator->Free(this->m_contents);
	}

private:
	FORCEINLINE T GetZeroedInstance() const
	{
		T elem;
		memset(&elem, 0, sizeof(T));
		return elem;
	}

	FORCEINLINE void InternalResize(length_t _size)
	{
		if (m_contents)
			m_contents = (T*)m_allocator->Reallocate(m_contents, sizeof(T) * _size);
		else
			m_contents = (T*)m_allocator->Reallocate(m_contents, sizeof(T) * _size);

		if (m_contents == NULL)
			throw std::exception();

		this->m_size = _size;
	}

public:

	FORCEINLINE void Push(const T& elem)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		if (m_top + 1 < m_size && m_contents)
		{
			m_contents[m_top + 1] = elem;
			m_top++;
		}
	}

	FORCEINLINE void Push(const Stack<T, I>& other)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		if (m_top + 2 + other.m_top < m_size && m_contents)
		{
			for (index_t i = m_top + 1, b = 0; i < m_size; i++)
			{
				m_contents[i] = other.m_contents[b];
				b++;
			}
		}
	}

	FORCEINLINE void Push(const T* elems, length_t len)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		LLIB_ASSERT(elems != NULL);
		LLIB_ASSERT(len > 0);

		if (m_top + 1 + len < m_size)
		{
			for (index_t i = m_top + 1, b = 0; i < m_size; i++)
			{
				m_contents[i] = elems[b];
				b++;
			}
		}
	}

	FORCEINLINE void Push(const Array<T, I>& other)
	{
		Push(other.GetElements(), other.GetSize());
	}

	FORCEINLINE T Pop()
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		if (m_top < 0)
			return GetZeroedInstance();

		T _top = m_contents[m_top];

		m_top--;

		return _top;
	}

	FORCEINLINE void PopAll() noexcept
	{
		m_top = 0;
	}

	FORCEINLINE void Insert(index_t index, const T& elem)
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		LLIB_ASSERT(index > 0 && index <= m_top);

		if (index <= m_top && index > 0)
		{
			m_contents[index] = elem;
			return;
		}
	}

	FORCEINLINE void Insert(index_t index, const T&& elem)
	{
		Insert(std::move(elem));
	}

	FORCEINLINE T GetTop() const
	{
		LLIB_ASSERT(m_contents);

		if (!m_contents)
			throw std::bad_alloc();

		if (m_top < 0)
			return GetZeroedInstance();
		return m_contents[m_top];
	}

	FORCEINLINE length_t GetCount() const noexcept
	{
		return m_top + 1;
	}

	FORCEINLINE length_t GetSize() const noexcept
	{
		return m_size;
	}

	FORCEINLINE void Resize(size_t _size)
	{
		if(_size > 0 && _size > this->m_size)
			InternalResize(_size);

		LLIB_ASSERT(m_contents);

		if (m_contents == NULL)
			throw std::exception();
	}

	FORCEINLINE bool Empty() const
	{
		return m_top == 0;
	}

	FORCEINLINE bool Equals(const Stack<T,I>& other) const
	{
		if(other.m_size == this->m_size && other.m_top == this->m_top && other.m_contents == this->m_contents)
			return true;
		return false;
	}

	FORCEINLINE Stack<T, I>& operator=(const Stack<T, I>& other)
	{
		this->m_allocator = other.m_allocator;
		this->m_top = other.m_top;
		this->m_size = other.m_size;
		m_allocator->CopyMemory(this->m_contents, other.m_contents, other.m_size);
		return *this;
	}

	FORCEINLINE bool operator==(const Stack<T, I>& other)
	{
		return Equals(other);
	}

	FORCEINLINE bool operator!=(const Stack<T,I>& other)
	{
		return !Equals(other);
	}
};

LLIB_HEADER_END