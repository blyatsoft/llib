/*

HybridArray.h

A hybrid array structure.
This structure is basically a linked list of fixed arrays.

----------
|  Arr1  |
|        |
|  arr2* |
|        |
----------

----------
|  Arr2  |
|        |
|  arr3* |
|        |
----------

*/
#pragma once

#include "../LLib.h"
#include "Allocator2.h"

///////////////////////////////
// Standard includes
#include <utility>
///////////////////////////////


LLIB_HEADER_START

template<class T, class I = int, int NBlockSize = 256>
class HybridArray
{
private:
	//Size of block should be NBlockSize
	//Block is a fixed size datatype
	class Block
	{
	public:
		T BasePointer[NBlockSize];
		I FreeIndices[NBlockSize];
		Block* pNext = NULL;
		Block* pPrev = NULL;
		I Index;

		bool bHasFree; //TODO: Figure out better way to do this? If I is unsigned, Index will always indicate a space in the array is free

		Block()
		{
			bHasFree = true;
			Index = NBlockSize-1;
			for(I i = 0, b = NBlockSize; i < NBlockSize; i++, b--)
			{
				FreeIndices[i] = b;
			}
		}

		Block(Block* prev, Block* next) :
			Block()
		{
			pPrev = prev;
			pNext = next;

			if(pPrev)
				pPrev->pNext = this;
			if(pNext)
				pNext->pPrev = this;
		}

		//Returns true if array has free space, false if not
		FORCEINLINE bool HasFree(I& free)
		{
			if(!bHasFree)
				return false;
			if(Index > 0)
			{
				free = FreeIndices[Index];
				Index--;

				if(Index == 0)
					bHasFree = false;

				return true;
			}
			else
				return false;
		}

		FORCEINLINE void Free(I index)
		{
			Index++;
			FreeIndices[Index] = index;
			bHasFree = true;
		}
	};

	///////////////////////////////////////////////////
	// Variables
	Block* m_pBasePointer = NULL;

	size_t m_nBlocks = 0;

	IAllocator2& m_allocator;
	///////////////////////////////////////////////////


	//Allocates a new block and shoves it into the list
	FORCEINLINE Block* Internal_AllocateBlock(Block* prev, Block* next)
	{
		m_nBlocks++;
		Block* blk = (Block*)m_allocator.Allocate(sizeof(Block));
		blk = Block(prev, next);
		return blk;
	}

	FORCEINLINE void Internal_DestroyBlock(I blk_index)
	{
		//Iterate through all blocks
		Block* pCurrent = m_pBasePointer;
		for(int i = 0; i < m_nBlocks && pCurrent; i++)
		{
			if(i == blk_index)
			{
				if(pCurrent->pPrev)
					pCurrent->pPrev->pNext = NULL;
				if(pCurrent->pNext)
					pCurrent->pNext->pPrev = NULL;
				delete pCurrent;
				m_nBlocks--;
				return;
			}
			else
				pCurrent = pCurrent->pNext;
		}
	}

	FORCEINLINE void Internal_DestroyAll()
	{
		//Destroys all blocks
		for(Block* pCurrent = m_pBasePointer; pCurrent;)
		{
			Block* next = pCurrent->pNext;
			m_allocator.Free(pCurrent);
			pCurrent = next;
		}
		m_nBlocks = 0;
	}

	FORCEINLINE T* Internal_BuyAllocation()
	{
		Block* pCurrent = m_pBasePointer;
		for(int i = 0; i < m_nBlocks && pCurrent; i++, pCurrent = pCurrent->pNext)
		{
			I index = 0;
			if(pCurrent->HasFree(index))
				return &pCurrent->BasePointer[index];
			else if(pCurrent->pNext == NULL)
			{
				//Allocate new block, need to copy local object into allocated space because Allocate wont call constructor
				Block* mem = (Block*)m_allocator.Allocate(sizeof(Block));
				Block blk = Block(pCurrent, NULL);
				*mem = blk;
				pCurrent->pNext = mem;
				m_nBlocks++;
			}
		}
	}

	FORCEINLINE bool Internal_Insert(const T& elem, I index)
	{
		Block* pCurrent = m_pBasePointer;
		I block_index = (I)index/NBlockSize; //35/32 = 1

		//Loops until i = block_index, then we have the current block
		for(I i = 0; i < block_index && pCurrent; i++, pCurrent = pCurrent->pNext);

		if(pCurrent)
		{
			pCurrent->BasePointer[index-(block_index*NBlockSize)-1] = elem; //35 - (1*32) = 3-1 = 2 = index
			return true;
		}

		return false;
	}

	FORCEINLINE bool Internal_Add(const T& elem)
	{
		T* ptr = Internal_BuyAllocation();
		if(ptr)
		{
			(*ptr) = elem;
		}
		else
			return false;
		return true;
	}

	FORCEINLINE void Internal_PopulateFromArray(T* ptr, I len)
	{
		for(int i = 0; i < len; i++)
		{
			(*Internal_BuyAllocation()) = ptr[i];
		}
	}

	//Copies blocks into this structure, also deletes all stored data
	FORCEINLINE void Internal_CopyBlocks(const Block* pBasePtr)
	{
		Internal_DestroyAll();
		Block* pNewBase, pPrev = NULL;

		for(const Block* pCurrent = pBasePtr; pCurrent; pCurrent = pCurrent->pNext)
		{
			pNewBase = (Block*)memcpy(m_allocator.Allocate(sizeof(Block)), pCurrent, sizeof(Block));

			if(pNewBase)
				pNewBase->pPrev = pPrev;
			if(pPrev)
				pPrev->pNext = pNewBase;

			pPrev = pNewBase;
		}
	}

public:

	/*
	Default constructor
	*/
	HybridArray(IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		this->m_allocator = *allocator;
	}

	/*
	Copy constructor
	*/
	HybridArray(const HybridArray<T,I,NBlockSize>& other, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		this->m_allocator = *allocator;
		Internal_CopyBlocks(other.m_pBasePointer);
	}

	/*
	Move constructor
	*/
	HybridArray(HybridArray<T,I,NBlockSize>&& other)
	{
		this->m_allocator = other.m_allocator;
		this->m_nBlocks = other.m_nBlocks;
		this->m_pBasePointer = other.m_pBasePointer;
		other.m_pBasePointer = NULL;
		other.m_nBlocks = 0;
		other.m_allocator = NULL;
	}

	/*
	Construct around native array
	*/
	HybridArray(const T* array, I length, IAllocator2* allocator = DefaultAllocator2::GetInstance())
	{
		this->m_allocator = *allocator;
		Internal_PopulateFromArray(array, length);
	}

public:

	FORCEINLINE void Add(const T& elem)
	{
		Internal_Add(elem);
	}

	FORCEINLINE void Add(T&& elem)
	{
		this->Add(std::move(elem));
	}

	FORCEINLINE bool Contains(const T& elem) const
	{
		return false;
	}

	FORCEINLINE void Insert(const T& elem, I index)
	{

	}

	FORCEINLINE void Insert(T&& elem, I index)
	{

	}

	FORCEINLINE void Remove(const T& elem, I count = 1)
	{

	}

	FORCEINLINE void Replace(const T& old, const T& New)
	{

	}

	FORCEINLINE void Replace(const T& old, T&& New)
	{
		this->Replace(old, std::move(New));
	}

	FORCEINLINE I GetCount() const
	{

	}

	FORCEINLINE I GetCapacity() const
	{

	}

	FORCEINLINE constexpr int GetBlockSize() const
	{
		return NBlockSize;
	}

	FORCEINLINE void Destroy()
	{

	}

	FORCEINLINE void AddRange(const HybridArray<T,I,NBlockSize>& other)
	{

	}

	FORCEINLINE void AddRange(const T* arr, I len)
	{

	}

	FORCEINLINE I IndexOf(const T& elem) const
	{

	}

	FORCEINLINE I FirstIndexOf(const T& elem) const
	{

	}

	FORCEINLINE I LastIndexOf(const T& elem) const
	{

	}

	FORCEINLINE void Empty()
	{

	}

	FORCEINLINE bool Equals(const HybridArray<T,I,NBlockSize>& other) const
	{

	}

	FORCEINLINE bool Equals(const T* ptr, I len) const
	{

	}

	/*
	Removes all excess allocated space in an array.
	Warning: SLOW
	*/
	FORCEINLINE void Trim()
	{

	}

	/*

	FORCEINLINE FixedArray FindAll(const T& elem) {}

	FORCEINLINE FixedArray FindAllIndices(const T& elem) {}

	ToFixedArray

	ToStack

	ToQueue

	ToList

	FromStack

	FromQueue

	FromFixedArray

	FromList



	*/

public:

	bool operator==(const HybridArray<T,I,NBlockSize>& other) const
	{

	}

	bool operator!=(const HybridArray<T,I,NBlockSize>& other) const
	{

	}

};

LLIB_HEADER_END
