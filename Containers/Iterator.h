/*

Iterator.h

Defines iterator types

*/
#pragma once

#include "LLib.h"

#include "Array.h"
#include "List.h"
#include "Stack.h"



///////////////////////
//Forward decls
//template<class T, class I = int, class _Allocator = DefaultAllocator>
//class Array;
template<class T, class I, class _Allocator>
class List;
template<class T, class I, class _Allocator>
class Stack;

///////////////////////

LLIB_HEADER_START


template<class T>
class LLIB_HEADER_API IIterator
{
public:
	virtual const T* GetCurrent() const = 0;
};

template<class T>
class LLIB_HEADER_API IForwardIterator : public IIterator<T>
{

};

template<class T, class I>
class LLIB_HEADER_API IBiDirectionalIterator : public IIterator<T>
{
public:
	virtual bool HasNext() const = 0;
	virtual bool HasPrevious() const = 0;
	virtual bool HasCurrent() const = 0;
	virtual void Advance() = 0;
	virtual void Reverse() = 0;
	virtual T* Next() = 0;
	virtual T* Previous() = 0;
	virtual T* Current() const = 0;
};

template<class T, class I>
class LLIB_HEADER_API IConstBiDirectionalIterator : public IIterator<T>
{
public:
	virtual bool HasNext() const = 0;
	virtual bool HasPrevious() const = 0;
	virtual bool HasCurrent() const = 0;
	virtual void Advance() = 0;
	virtual void Reverse() = 0;
	virtual const T* Next() = 0;
	virtual const T* Previous() = 0;
	virtual const T* Current() const = 0;
};

template<class T, class I>
class LLIB_HEADER_API BiDirectionalIterator : IBiDirectionalIterator<T, I>
{

};

/*

*/
template<class T, class I>
class LLIB_HEADER_API ForwardIterator : IForwardIterator<T>
{

};





/*

class ArrayIterator<T, I, A>

Iterator for array class

*/
template<class T, class I>
class LLIB_HEADER_API ArrayIterator final : IBiDirectionalIterator<T, I>
{
private:
	//Array should be a friend
	template<T, I>
	friend class Array;

private:
	Array<T, I>* m_array;

	I m_index = 0;

	ArrayIterator() {};

public:
	//Constructs this ArrayIterator object around the specified Array
	ArrayIterator(Array<T, I>& other)
	{
		m_array = &other;
	}

	~ArrayIterator()
	{

	}

	FORCEINLINE virtual bool HasNext() const override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
				return true;
		}

		return false;
	}

	FORCEINLINE virtual bool HasCurrent() const override
	{
		return m_array->m_contents[m_index].free != true;
	}

	FORCEINLINE virtual bool HasPrevious() const override
	{
		if (m_index <= 0)
			return false;

		for (I i = m_index; i < m_array->m_size; i--)
		{
			if (!m_array->m_contents[i].free)
				return true;
		}

		return false;
	}

	FORCEINLINE virtual void Advance() override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return;
			}
		}
	}

	FORCEINLINE virtual void Reverse() override
	{
		if (m_index <= 0)
			return;

		for (I i = m_index; i < m_array->m_size; i--)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return;
			}
		}
	}

	FORCEINLINE virtual T* Next() override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return &m_array->m_contents[i].elem;
			}
		}

		return NULL;
	}

	FORCEINLINE virtual T* Previous() override
	{
		for (I i = m_index; i < m_array->m_size; i--)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return &m_array->m_contents[i].elem;
			}
		}

		return NULL;
	}

	FORCEINLINE virtual T* Current() const override
	{
		if (!m_array->m_contents[m_index].free)
			return &m_array->m_contents[m_index].elem;

		return NULL;
	}

	FORCEINLINE void operator++()
	{
		Advance();
	}

	FORCEINLINE void operator++(int)
	{
		Advance();
	}

	FORCEINLINE void operator--()
	{
		Reverse();
	}

	FORCEINLINE void operator--(int)
	{
		Reverse();
	}

	FORCEINLINE ArrayIterator<T, I>& operator=(const ArrayIterator<T, I>& other)
	{
		ArrayIterator<T, I> _new = ArrayIterator<T, I>();
		_new.m_index = other.m_index;
		_new.m_array = other.m_array;
		return _new;
	}
};





/*

ConstArrayIterator<T, I, A>

Iterator for constant arrays

 */
template<class T, class I>
class LLIB_HEADER_API ConstArrayIterator final : IConstBiDirectionalIterator<T, I>
{
private:
	//Array should be a friend
	template<T, I>
	friend class Array;

private:
	const Array<T, I>* m_array;

	I m_index = 0;

	ConstArrayIterator() {};

public:
	//Constructs this ArrayIterator object around the specified Array
	ConstArrayIterator(const Array<T, I>& other)
	{
		m_array = &other;
	}

	~ConstArrayIterator()
	{

	}

	FORCEINLINE virtual bool HasNext() const override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
				return true;
		}

		return false;
	}

	FORCEINLINE virtual bool HasCurrent() const override
	{
		return m_array->m_contents[m_index].free != true;
	}

	FORCEINLINE virtual bool HasPrevious() const override
	{
		if (m_index <= 0)
			return false;

		for(I i = m_index; i < m_array->m_size; i--)
		{
			if (!m_array->m_contents[i].free)
				return true;
		}

		return false;
	}

	FORCEINLINE virtual void Advance() override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return;
			}
		}
	}

	FORCEINLINE virtual void Reverse() override
	{
		if (m_index <= 0)
			return;

		for (I i = m_index; i < m_array->m_size; i--)
		{
			if(!m_array->m_contents[i].free)
			{
				m_index = i;
				return;
			}
		}
	}

	FORCEINLINE virtual const T* Next() override
	{
		for (I i = m_index; i < m_array->m_size; i++)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return &m_array->m_contents[i].elem;
			}
		}

		return NULL;
	}

	FORCEINLINE virtual const T* Previous() override
	{
		for (I i = m_index; i < m_array->m_size; i--)
		{
			if (!m_array->m_contents[i].free)
			{
				m_index = i;
				return &m_array->m_contents[i].elem;
			}
		}

		return NULL;
	}

	FORCEINLINE virtual const T* Current() const override
	{
		if (!m_array->m_contents[m_index].free)
			return &m_array->m_contents[m_index].elem;

		return NULL;
	}

	void operator++()
	{
		Advance();
	}

	void operator++(int)
	{
		Advance();
	}

	void operator--()
	{
		Reverse();
	}

	void operator--(int)
	{
		Reverse();
	}

	ConstArrayIterator<T, I>& operator=(const ConstArrayIterator<T, I>& other)
	{
		ConstArrayIterator<T, I> _new = ConstArrayIterator<T, I>();
		_new.m_index = other.m_index;
		_new.m_array = other.m_array;
		return _new;
	}
};





/*

class ListIterator<T, I, A>

Iterator for list class

*/
template<class T, class I, class A>
class LLIB_HEADER_API ListIterator : IBiDirectionalIterator<T, I>
{

};

/*

class SingleListIterator<T, I, A>

Iterator for single list class

*/
template<class T, class I, class A>
class LLIB_HEADER_API SingleListIterator : IForwardIterator<T>
{

};

/*

class MapIterator<K, V, I, A>

Iterator for map class

*/
template<class T, class I, class A>
class LLIB_HEADER_API MapIterator : IBiDirectionalIterator<T, I>
{

};

/*

class StackIterator<T, I, A>



*/
template<class T, class I, class A>
class LLIB_HEADER_API StackIterator : public IBiDirectionalIterator<T, I>
{

};

LLIB_HEADER_END