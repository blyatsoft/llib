/*

Array.h

Various simple array structures

*/
#pragma once

#include <stddef.h>
#include <functional>

#include "Iterator.h"
#include "Allocator2.h"
#include "Stack.h"

/*

 Structure of the array class

 Due to challenges with implementing an efficient array, I've decided to think up a new array-like structure
 Turns out it's not much like an array, mostly. I'll call it the "hyrbid-array"
 Basically, elements of the structure are contained together but linked like a doubly linked array.

 In order to support removal of items this needs to be a doubly linked list.

 The structure works as follows:

 next* and prev* inside each element can be a "short" pointer, or an offset from the start of the block.
 This will use less memory than traditional linked lists, as each element doesn't maintain a pointer to the next element,
 rather it maintains the index of the next element.

 In the block container, next_block and previous_block are pointers to the next block of memory. If set to 0, it indicates the block is the start/end of the array
 ----------------
 |    Block1    |
 | ------------ |
 | | Element1 | |
 | |          | |
 | | next*    | |
 | | prev*    | |
 | ------------ |
 | ------------ |
 | | Element2 | |
 | |          | |
 | | next*    | |
 | | prev*    | |
 | ------------ |
 |  next_block* |
 |  prev_block* |
 ----------------

 PREVIOUS DESIGN SCRAPPED

 Another design for an array structure:

 A large block of memory can be allocated for storage of array elements. Each block maintains a pointer to the next block and the previous block.
 When more space for the array is allocated, it allocates a large block at once. This removes the need for realloc, which can have a huge overhead if memory needs
 to be copied to a new location.

*/

LLIB_HEADER_START

template<class T>
class FixedArray
{
private:
	const T* contents;
	size_t size;

	typedef long long index_t;
	typedef long long length_t;

public:
	/*
	Normal constructor..?
	*/
	FixedArray(const T* arr, size_t len)
	{
		contents = arr;
		size = len;
	}

	/*
	Copy constructor
	*/
	FixedArray(const FixedArray<T>& other)
	{
		contents = other.GetElements();
		size = other.GetSize();
	}

	/*
	Returns true if the array contains elem
	*/
	virtual bool Contains(const T& elem) const
	{
		for (length_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				return true;
		}
		return false;
	}

	/*
	Returns the maximum size of the array
	This does not equate to the number of elements, but rather the total allocated size
	*/
	virtual length_t GetSize() const
	{
		return size;
	}

	/*
	Returns the total number of elements in the array
	*/
	virtual length_t GetCount() const
	{
		length_t i;
		for (i = 0; contents[i]; i++);
		return i;
	}

	/*
	Returns a const pointer to the elements array
	*/
	virtual const T* GetElements() const
	{
		return contents;
	}

	/*
	Returns the index of elem, or -1 if not found.
	*/
	virtual index_t IndexOf(const T& elem) const
	{
		for (index_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				return i;
		}
	}

	/*
	Returns the last index of elem, or -1 if not found
	*/
	virtual index_t LastIndexOf(const T& elem) const
	{
		for (index_t i = size - 1; i >= 0; i++)
		{
			if (contents[i] == elem)
				return i;
		}
	}

	/*
	Returns the count of elem in the array
	*/
	virtual length_t GetCountOf(const T& elem) const
	{
		length_t count = 0;
		for (length_t i = 0; i < size; i++)
		{
			if (contents[i] == elem)
				count++;
		}
	}

	/*
	Copies the array
	*/
	virtual T* Copy() const
	{
		void* mem = std::malloc(size * sizeof(T));
		memcpy(mem, contents, sizeof(T) * size);
		return mem;
	}


	virtual const T* GetElement(index_t index) const
	{
		if (index >= size || index < 0)
			return nullptr;
		return &contents[index];
	}


	const T& operator[](index_t index) const
	{
		return *GetElement(index);
	}
};



/*

Array

A dynamially sized writable array

*/
template<class T, class I = int>
class Array /*: public IWritableArray<T>, IReadOnlyArray<T>*/
{
private:

	typedef I index_t;
	typedef I length_t;

	/*
	Represents an element in the array
	*/
	template<class _T>
	struct ArrayElement
	{
		bool free = true;
		T elem;
	};

	/*
	Actual contents of the array
	*/
	ArrayElement<T>* m_contents = NULL;
	
	/*
	Allocated length of the array
	*/
	length_t m_size = 0;

	/*
	First free index in the array, just so we dont have to 
	*/
	index_t m_freeindex = 0;

	/*
	Count of array elements
	*/
	length_t m_count = 0;

	/*
	Array allocator
	*/
	IAllocator2* m_allocator;


	/*
	Iterator should be a friend
	*/
	//template<T, I, _Allocator>
	friend class ArrayIterator<T, I>;

	bool* m_usedindicies;

protected:

	FORCEINLINE ArrayElement<T>* InternalCopy() const
	{
		//We do not use the allocator because we cannot guarentee that the allocator will be used to free this copy
		ArrayElement<T>* temp = (ArrayElement<T>*)malloc(sizeof(ArrayElement<T>) * m_size);
		return (ArrayElement<T>*)memcpy(temp, m_contents, sizeof(ArrayElement<T>) * m_size);
	}

	//Returns a zeroed instance of T
	FORCEINLINE T& GetZeroedInstance(unsigned count = 1) const
	{
		//T* elem = (T*)allocator.malloc(sizeof(T) * count);
		T elem;
		memset(&elem, 0, sizeof(T));
		return elem;
	}

	FORCEINLINE void InternalInsert(index_t index, const T& val)
	{
		m_contents[index].elem = val;
		m_contents[index].free = false;

		m_usedindicies[index] = 1;

		m_count++;
	}

	FORCEINLINE void InternalRemove(index_t index)
	{
		m_contents[index].free = true;
		m_freeindex = index;
		m_usedindicies[index] = 0;
		m_count--;
	}

	FORCEINLINE void InternalResize(length_t newsize)
	{
		if (m_contents)
		{
			m_contents = (ArrayElement<T> *) m_allocator->Reallocate(m_contents, sizeof(ArrayElement<T>) * newsize);
		}
		else
			m_contents = (ArrayElement<T>*)m_allocator->Allocate(sizeof(ArrayElement<T>) * newsize);

		//if(m_usedindicies)
		//	m_usedindicies = m_allocator->

		//InitializeArray(newsize-this->m_size);
		this->m_size = newsize;
	}

	FORCEINLINE void InitializeArray(index_t start = 0)
	{
		for (index_t i = start; i < m_size; i++)
			m_contents[i].free = true;
	}

	FORCEINLINE void VerifyContents() const
	{
		LLIB_ASSERT(m_contents);

		if (m_contents == NULL)
			throw std::bad_alloc();
	}

	FORCEINLINE void Initialize(length_t length)
	{
		//m_contents = m_allocator->Allocate(length * sizeof(ArrayElement<T>));
		//m_usedindicies = m_allocator->AllocateArray(length, sizeof(bool));
	}

public:

	Array(IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		InternalResize(1);

		Initialize(1);
	}

	Array(const T* elems, length_t len, IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		m_contents = (ArrayElement<T>*)m_allocator->AllocateArray(len, sizeof(ArrayElement<T>));

		for (length_t i = 0; i < len; i++)
		{
			m_contents[i].free = false;
			m_contents[i].elem = elems[i];
			m_count++;
		}

		m_size = len;

		m_usedindicies = m_allocator->AllocateArray(len, sizeof(bool));
		std::memset(m_usedindicies, 1, sizeof(bool) * len);
	}

	Array(const Array<T, I>& array)
	{
		m_allocator = array.m_allocator;

		m_contents = array.InternalCopy();
		
		m_size = array.GetSize();

		m_count = array.GetCount();

		m_usedindicies = array.m_usedindicies;
	}

	Array(length_t size, IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_allocator = alloc;

		InternalResize(size);

		Initialize(size);
	}

	~Array()
	{
		if(m_contents)
			m_allocator->Free(this->m_contents);
	}

	/*
	Creates a new iterator
	*/
	FORCEINLINE LLib::ArrayIterator<T, I> CreateIterator()
	{
		VerifyContents();
		return LLib::ArrayIterator<T, I>(*this);
	}

	/*
	Creates a new constant iterator
	*/
	FORCEINLINE LLib::ConstArrayIterator<T, I> CreateConstIterator() const
	{
		VerifyContents();
		return LLib::ConstArrayIterator<T, I>(*this);
	}


	/*
	Replaces _old with _new in the array
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void Replace(const T& _old, const T& _new, length_t _count = 1)
	{
		LLIB_ASSERT(m_count >= 0);

		VerifyContents();

		for (length_t i = 0; i < m_size && _count > 0; i++)
		{
			if (m_contents[i].elem == _old)
			{
				m_contents[i].elem = _new;
				m_count--;
				_count--;
			}
		}
	}

	/*
	Replace all instances of _old with _new
	*/
	FORCEINLINE virtual void ReplaceAll(const T& _old, const T& _new)
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].elem == _old)
			{
				m_contents[i].elem = _new;
			}
		}
	}

	/*
	Inserts elem into the specified index in the array, if index > size-1, elem will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void Insert(index_t index, const T& elem)
	{
		LLIB_ASSERT(index > 0);

		VerifyContents();


		if (index < m_size && m_contents[index].free)
		{
			InternalInsert(index, elem);
		}
	}

	/*
	Inserts values into the array starting at the specified index. If index > size, values will not be inserted into the array.
	Returns true on success, false otherwise
	*/
	FORCEINLINE virtual void InsertRange(index_t index, const Array<T, I>& values)
	{
		LLIB_ASSERT(index > 0);

		VerifyContents();


		if (index <= m_size)
		{
			length_t count1 = values.GetCount();
			length_t count2 = this->GetCount();

			if (count1 + count2 >= m_size)
			{
				this->Resize(count1 + count2);
				m_size = count1 + count2;
			}

			for (length_t i = count2; i < m_size; i++)
			{
				InternalInsert(i, values.GetElement(i));
			}
		}
	}

	/*
	Adds an element to the array and resizes if necessary.
	This operation is guarenteed to succeed regardless of the value of elem, but may fail if an allocation fails.
	*/
	FORCEINLINE virtual void Add(const T& elem)
	{
		VerifyContents();

		throw new std::exception();

	}

	/*
	Adds a range of elements to the array, resizes if necessary.
	*/
	FORCEINLINE virtual void AddRange(const Array<T>& values)
	{
		VerifyContents();

		index_t index = m_size;
		for (; index >= 0 && m_contents[index].free == false; index--)
			;
		InsertRange(index + 1, values);
	}

	/*
	Removes the element at the specified index. Operation fails if index > size-1
	*/
	FORCEINLINE virtual void Remove(index_t index)
	{
		LLIB_ASSERT(index >= 0);

		VerifyContents();


		if (index < m_size && m_contents[index].free == false)
		{
			InternalRemove(index);
		}
	}

	/*
	Removes elem from the array
	*/
	FORCEINLINE virtual void Remove(const T& elem, length_t count = 1)
	{
		LLIB_ASSERT(count >= 0);

		VerifyContents();


		for (length_t i = 0; i < m_size && count > 0; i++)
		{
			if (m_contents[i].elem == elem)
			{
				m_contents[i].free = true;
				count--;

				m_usedindicies[i] = false;
			}
		}
	}

	/*
	Removes all instances of elem from the array
	*/
	FORCEINLINE virtual void RemoveAll(const T& elem)
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].elem == elem)
			{
				InternalRemove(i);
			}
		}
	}

	/*
	Removes the values contained in the specified array from the array
	*/
	FORCEINLINE virtual void RemoveRange(const Array<T, I>& values)
	{
		VerifyContents();

		for (length_t i = 0; i < values.GetCount(); i++)
		{
			this->RemoveAll(values.GetElement(i));
		}
	}

	/*
	Sorts the array using the specified comparison function
	*/
	FORCEINLINE virtual void Sort(std::function<bool(const T&, const T&)> function)
	{
		VerifyContents();

	}

	/*
	Reserves size for the number of elements specified in the array.
	This should be called once after the initialization of an empty array.
	This operation will not occur if called more than once or if called after the array is initialized with values.
	*/
	FORCEINLINE virtual void Reserve(length_t size)
	{
		//TODO: Remove this function
		Resize(size);

		//LLIB_ASSERT(contents);
	}

	/*
	Resizes the array.
	Should be called only after the array has either been initialized with values or had space reserved inside of it.
	*/
	FORCEINLINE virtual void Resize(length_t _size)
	{
		LLIB_ASSERT(m_size > 0);

		VerifyContents();


		if (_size > this->m_size)
		{
			InternalResize(_size);
		}

		LLIB_ASSERT(m_contents);
	}

	/*
	Returns true if the array contains elem
	*/
	FORCEINLINE virtual bool Contains(const T& elem) const
	{
		VerifyContents();

		for (length_t i = 0; i < m_size; i++)
		{
			if (!m_contents[i].free && m_contents[i].elem == elem)
				return true;
		}
		return false;
	}

	/*
	Returns the maximum size of the array
	This does not equate to the number of elements, but rather the total allocated size
	*/
	FORCEINLINE virtual length_t GetSize() const noexcept
	{
		return m_size;
	}

	/*
	Returns the total number of elements in the array
	*/
	FORCEINLINE virtual length_t GetCount() const noexcept
	{
		return m_count;
	}

	/*
	Returns a const pointer to the elements array
	*/
	FORCEINLINE virtual const T* GetElements() const
	{
		//TODO: Find a better way to do this horrible thing
		return this->Copy();
	}

	/*
	Returns the index of elem, or -1 if not found.
	*/
	FORCEINLINE index_t IndexOf(const T& elem) const
	{
		VerifyContents();

		for(index_t i = 0; i < m_size; i++)
		{
			if (!m_contents[i].free && m_contents[i].elem == elem)
				return i;
		}
		return -1;
	}

	/*
	Returns the last index of elem, or -1 if not found
	*/
	FORCEINLINE index_t LastIndexOf(const T& elem) const
	{
		VerifyContents();

		for (index_t i = m_size-1; i > 0; i--)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
				return i;
		}
		return -1;
	}

	/*
	Returns a read only array of pointers to the instances of elem in the array
	*/
	FORCEINLINE virtual Array<T, I> GetInstancesOf(const T& elem) const
	{
		VerifyContents();

		Array<T, I> ret(1);

		for (index_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
			{
				ret.Add(m_contents[i].elem);
			}
		}

		return ret;
	}

	/*
	Returns the count of elem in the array
	*/
	FORCEINLINE virtual length_t GetCountOf(const T& elem) const
	{
		VerifyContents();

		length_t ecount = 0;
		for (length_t i = 0; i < m_size; i++)
		{
			if (m_contents[i].free == false && m_contents[i].elem == elem)
				ecount++;
		}
		return ecount;
	}

	/*
	Copies the array
	*/
	FORCEINLINE virtual T* Copy() const
	{
		VerifyContents();

		//Allocator isn't being used to allocate this as we are releasing the pointer into the wild,
		//and the allocator of the current object isn't guaranteed to be used in the free-ing of this pointer
		T* temp = (T*)malloc(sizeof(T) * m_size);

		for (length_t i = 0; i < m_size; i++)
		{
			if(m_contents[i].free)
				temp[i] = m_contents[i].elem;
		}

		return temp;

	}

	/*
	Checks if this array is equal to another array
	*/
	FORCEINLINE virtual bool Equals(const Array<T, I> other) const
	{
		VerifyContents();

		for (index_t i = 0; i < m_size; i++)
		{
			if (other.GetElement(i) != m_contents[i].elem)
				return false;
		}
	}

	/*
	Returns the element at the specified index
	*/
	FORCEINLINE virtual T GetElement(index_t index) const
	{
		LLIB_ASSERT(index >= 0);

		VerifyContents();

		if (index < m_size && m_contents[index].free == false)
			return m_contents[index].elem;
		
		return GetZeroedInstance(1);
	}

	FORCEINLINE virtual T GetLastElement() const
	{
		VerifyContents();

		for (index_t i = m_size - 1; i >= 0; i++)
		{
			if (m_contents[i].free == false)
				return m_contents[i].elem;
		}
		return GetZeroedInstance(1);
	}

	FORCEINLINE virtual bool Empty() const noexcept
	{
		return this->m_count == 0;
	}

	FORCEINLINE virtual bool IsValid() const noexcept
	{
		return m_contents != NULL;
	}

	Array<T,I>& operator=(const Array<T,I>& other)
	{
		this->m_size = other.m_size;
		this->m_allocator = other.m_allocator;
		//this->m_freestack = other.m_freestack;
		this->m_freeindex = other.m_freeindex;
		this->m_contents = other.InternalCopy();
		return *this;
	}

	bool operator==(const Array<T, I>& other) const
	{
		return Equals(other);
	}

	bool operator!=(const Array<T, I>& other) const
	{
		return !Equals(other);
	}

	T& operator[](int index)
	{
		T* tmp = this->GetElement(index);
		if (tmp != NULL)
			return tmp;
		else
			return this->GetZeroedInstance(1);
	}

	void operator>>(int shift)
	{
		
	}


};

LLIB_HEADER_END