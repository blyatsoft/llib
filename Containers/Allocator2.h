/*
 *
 * Allocator2.h
 *
 * Revised allocator
 *
 */
#pragma once

#include <LLib.h>

#include <memory>
#include <cstring>

LLIB_HEADER_START


class IAllocator2
{
public:
	virtual void* Allocate(size_t size) = 0;

	virtual void* Reallocate(void* block, size_t newsize) = 0;

	virtual void Free(void* block) = 0;

	virtual void* AllocateArray(size_t num, size_t size) = 0;

	virtual void ZeroMemory(void* block, size_t length) = 0;

	virtual void CopyMemory(void* dest, void* src, size_t length) = 0;

	static IAllocator2* GetInstance() { return nullptr; };
};

template<IAllocator2* allocator>
class Test
{

};

class DefaultAllocator2 final : public IAllocator2 {
private:
	static DefaultAllocator2 *m_pDefaultAllocator;
public:
	virtual void *Allocate(size_t size)
	{
		return std::malloc(size);
	}

	virtual void *Reallocate(void *block, size_t newsize)
	{
		return std::realloc(block, newsize);
	}

	virtual void Free(void *block)
	{
		std::free(block);
	}

	virtual void *AllocateArray(size_t num, size_t size)
	{
		return std::calloc(num, size);
	}

	virtual void ZeroMemory(void* block, size_t length)
	{
		std::memset(block, 0, length);
	}

	virtual void CopyMemory(void* dest, void* src, size_t length)
	{
		std::memcpy(dest, src, length);
	}

	static IAllocator2* GetInstance()
	{
		if(!m_pDefaultAllocator)
			m_pDefaultAllocator = new DefaultAllocator2();
		return m_pDefaultAllocator;
	}
};

/*
* This class maintains a block of memory for an array to use
*/
template<class Type, unsigned short nBlockSize = 256>
class ArrayBlockManager
{
private:
	typedef unsigned short ushort;


	/*
	 * Constexpr to fill a fixed array
	 */
	template<int N>
	constexpr ushort* FillArr()
	{
		ushort arr[N];
		for(int i = N, b = 0; i > 0; i--, b++)
			arr[i] = (ushort)i;
		return arr;
	}



	ushort m_FreeDescriptors[nBlockSize] = FillArr();

	int m_Top = (ushort)(nBlockSize - 1);

	Type* m_Block = NULL;

	IAllocator2* m_Allocator = NULL;

public:
	ArrayBlockManager(IAllocator2* alloc = DefaultAllocator2::GetInstance())
	{
		m_Allocator = alloc;

		m_Block = m_Allocator->AllocateArray(nBlockSize, sizeof(Type));
	}

	ArrayBlockManager(ArrayBlockManager<Type,nBlockSize>&& other)
	{
		this->m_Block = other.m_Block;
		this->m_FreeDescriptors = other.m_FreeDescriptors;
		this->m_Allocator = other.m_Allocator;
		this->m_Top = other.m_Top;
		other.m_Block = NULL;
		other.m_Top = -1;
	}

	~ArrayBlockManager()
	{
		if(m_Block)
			m_Allocator->Free(m_Block);
	}

	FORCEINLINE Type* BuyAllocation()
	{
		if(m_Top < 0) //This block is filled
			return NULL;
		else
		{
			m_Top--;
			return &m_Block[m_FreeDescriptors[m_Top]];
		}
	}

	FORCEINLINE bool HasSpace() const
	{
		return m_Top >= 0;
	}

	FORCEINLINE void Destroy()
	{
		m_Allocator->Free(m_Block);
		m_Block = NULL;
	}

	FORCEINLINE ArrayBlockManager<Type, nBlockSize>& operator=(ArrayBlockManager<Type, nBlockSize>&& other) noexcept
	{
		this->m_Block = other.m_Block;
		this->m_FreeDescriptors = other.m_FreeDescriptors;
		this->m_Allocator = other.m_Allocator;
		this->m_Top = other.m_Top;
		other.m_Block = NULL;
		other.m_Top = -1;
		return *this;
	}
};

//nBlockSize cannot be > USHORT_MAX (or 65536)
template<class Type, unsigned short nBlockSize = 256, unsigned short nMaxBlocks = 256>
class ArrayAllocationManager
{
private:

#ifdef _MEMORY_DONT_CARE
	ArrayBlockManager<Type, nBlockSize> m_BlockManagers[nMaxBlocks];
#else
	ArrayBlockManager<Type, nBlockSize>* m_BlockManagers;
#endif

public:
};

/*

Allocators and allocation policies

Allocation policies are special classes that can be used to determine how much memory something allocates
The class specialization is relatively easy too.

*/
class IAllocationPolicy
{
public:
	//
	// Gets the next count of elements that should be allocated
	// For example: if one element should be allocated, this function should return 1
	//
	virtual size_t GetNextCount(size_t current_elems) = 0;
};

class LogarithmicAllocationPolicy : public IAllocationPolicy
{
public:
	virtual size_t GetNextCount(size_t current_elems);
};

//
// "Standard" policy, this by default doubles in size
//
class StandardAllocationPolicy : public IAllocationPolicy
{
public:
	virtual size_t GetNextCount(size_t current_elems)
	{
		return current_elems * 2;
	}
};

//
// Exponential policy. Squares the number of elements.
//
class ExponentialAllocationPolicy : public IAllocationPolicy
{
public:
	virtual size_t GetNextCount(size_t current_elems)
	{
		return current_elems * current_elems;
	}
};

//
// Simple policy, allocates only one extra element each time
//
class SimpleAllocationPolicy : public IAllocationPolicy
{
public:
	virtual size_t GetNextCount(size_t current_elems)
	{
		return current_elems++;
	}
};

/*

Memory blocks

Memory blocks basically hold a block of memory for later use
These use Allocators, allocation policies and can be fixed.

*/

//
// Base for all memory blocks
//
template<class T>
class IMemoryBlock
{
public:
	//
	// Returns or allocates a block of mem
	//
	virtual T* GetOrAllocate(size_t num = 1) = 0;

	//
	// Frees a pointer of T
	//
	virtual T* Free(T* ptr) = 0;

	//
	// Returns a block of memory or NULL if none are available
	//
	virtual T* GetBlock(size_t num = 1) = 0;

	//
	// Returns if there are any more spaces
	//
	virtual bool HasSpace() const = 0;

	//
	// Forcibly resizes the amount of memory held by this block
	//
	virtual void Resize(size_t new_num) = 0;

	//
	// Returns the number of blocks
	//
	virtual void GetSpace() const = 0;

	//
	// Disposes of all memory held by the allocator
	//
	virtual void Dispose() = 0;
};

//
// Fixed-size memory block
//
template<class T, class Policy>
class FixedMemoryBlock : public IMemoryBlock<T>
{
private:
	T* m_pBlock;

public:
	FixedMemoryBlock(size_t size)
	{
		
	}

	FixedMemoryBlock(const FixedMemoryBlock& other)
	{

	}

	FixedMemoryBlock(FixedMemoryBlock&& other)
	{

	}

	~FixedMemoryBlock()
	{

	}
};

//
// Normal growable memory block
//
template<class T, class Policy>
class MemoryBlock : public IMemoryBlock<T>
{
private:

public:

};
LLIB_HEADER_END
