/*

Vector3.h

Vector 3 stuff

*/
#pragma once

#include "Vector4.h"
#include "../LLib.h"

///////////////////////////////////
// Forward Declarations
template<class I>
class TVector4;
///////////////////////////////////

#ifdef _WINDOWS
#pragma pack(push, 1)
#endif

#include <math.h>

template<class I>
class _VECTOR_ALIGN TVector3
{
public:
	I x, y, z;

#ifdef _SIMD_VECTORS
	I _dummy = 0; // Extra to pad this, so we can load into a __m128 using _mm_load_ps1(&vector)
				  // NOTE: THIS MUST ALSO BE ZERO
#endif

	//
	// Default constructor
	// x, y, z all set to 0
	//
	TVector3()
	{
		x = (I)0;
		y = (I)0;
		z = (I)0;
	}

	//
	// Constructor
	//
	TVector3(I x, I y, I z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	//
	// Copy constructor
	// - other = other vector
	//
	TVector3(const TVector3<I>& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
	}

	//
	// Returns the X component of this vector
	//
	I GetX() const { return x; }

	//
	// Returns the Y component of this vector
	//
	I GetY() const { return y; }

	//
	// Returns the Z component of this vector
	//
	I GetZ() const { return z; }

	//
	// Sets the X component of this vector
	//
	void SetX(I x) { this->x = x; }

	//
	// Sets the Y component of this vector
	//
	void SetY(I y) { this->y = y; }

	//
	// Sets the Z component of this vector
	//
	void SetZ(I z) { this->z = z; }

	//
	// Computes the cross product between this vector and another
	//
	TVector3<I> Cross(const TVector3<I>& other) const
	{
		return TVector3<I>(y*other.z - z * other.y, z*other.x - x * other.z, x*other.y - y * other.x);
	}

	//
	// Computes the dot product between this vector and another vector
	//
	I Dot(const TVector3<I>& other) const
	{
		return (I)(x * other.x + y * other.y + z * other.z);
	}

	//
	// Computes the distance between this vector and another vector
	//
	I Distance(const TVector3<I>& other) const
	{
		return (I)sqrt((double)(((x - other.x) * (x - other.x)) + ((y - other.y)*(y - other.y)) + ((z - other.z)*(z - other.z))));
	}

	//
	// Computes the midpoint between this vector and another
	//
	TVector3<I> Midpoint(const TVector3<I>& other) const
	{
		return TVector3<I>((x + other.x) / 2, (y + other.y) / 2, (z + other.z) / 2);
	}

	//
	// Computes the magnitude of this vector
	//
	I Magnitude() const
	{
		return (I)sqrt((double)(x*x + y * y + z * z));
	}

	//
	// Returns a normalized 4 component version of this vector
	//
	TVector4<I> Normalize() const
	{
		TVector4<I> vec = TVector4<I>();

		I mag = Magnitude();

		vec.x = x / mag;
		vec.y = y / mag;
		vec.z = z / mag;
		vec.m = mag;

		return vec;
	}

	//
	// Performs a linear interpolation between this vector and another vector with the specified bias
	//
	TVector3<I> Lerp(const TVector3<I>& other, float bias) const
	{
		return (*this) * bias + other * (1.0f - bias);
	}
} _VECTOR_PACKED_CLASS;

class _VECTOR_ALIGN Vector3f
{
public:
	float x, y, z;
	
#ifdef _SIMD_VECTORS
	float dummy = 1; // Used as class padding so it can be loaded as a single __m128
#endif
public:
	
	Vector3f()
	{
	}
	
	Vector3f(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	
	Vector3f(const Vector3f& vec)
	{
#ifdef _SIMD_VECTORS
		_mm_store_ps((float*)this, _mm_load_ps((float*)&vec));
#else
		this->x = vec.x;
		this->y = vec.y;
		this->z = vec.z;
#endif
	}
	
public:
	
	/*
	 * Normalize the vector in place, return 
	 */ 
	FORCEINLINE float Normalize()
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	/*
	 * Returns dot product of this vector and another 
	 */ 
	FORCEINLINE float DotProduct(const Vector3f& vec) const
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
public:
	/*
	 * Cross product of two vectors
	 */ 
	FORCEINLINE Vector3f CrossProduct(const Vector3f& vec) const
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
public:
	FORCEINLINE void Mul(float c)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		_mm_store_ps((float*)this, _mm_mul_ps(v1, _mm_set1_ps(c)));
#else
		
#endif
	}
	
	FORCEINLINE void Div(float c)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		_mm_store_ps((float*)this, _mm_mul_ps(v1, _mm_rcp_ps(_mm_set1_ps(c)))); /* rcp/mul used due to less latency */
#else
		
#endif
	}
	
	FORCEINLINE void Add(float c)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		_mm_store_ps((float*)this, _mm_add_ps(v1, _mm_set1_ps(c)));
#else
		
#endif
	}
	
	FORCEINLINE void Sub(float c)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		_mm_store_ps((float*)this, _mm_sub_ps(v1, _mm_set1_ps(c)));		
#else
		
#endif
	}

public:
	FORCEINLINE void Add(const Vector3f& vec)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		__m128 v2 = _mm_load_ps((float*)&vec);
		_mm_store_ps((float*)this, _mm_add_ps(v1, v2));
#else
		
#endif
	}
	
	FORCEINLINE void Sub(const Vector3f& vec)
	{
#ifdef _SIMD_VECTORS
		__m128 v1 = _mm_load_ps((float*)this);
		__m128 v2 = _mm_load_ps((float*)&vec);
		_mm_store_ps((float*)this, _mm_sub_ps(v1, v2));
#else
		
#endif
	}

public:
	/*
	 * 
	 * Static utils
	 * 
	 */ 
	FORCEINLINE static Vector3f Add(const Vector3f& vec1, const Vector3f& vec2)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_div_ps(_mm_load_ps((float*)&vec1), _mm_load_ps((float*)&vec2)));
#else
		
#endif
	}
	
	FORCEINLINE static Vector3f Sub(const Vector3f& vec1, const Vector3f& vec2)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_div_ps(_mm_load_ps((float*)&vec1), _mm_load_ps((float*)&vec2)));
#else
		
#endif
	}
	
	FORCEINLINE static Vector3f Add(const Vector3f& vec1, float c)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_add_ps(_mm_load_ps((float*)&vec1), _mm_set1_ps(c)));
#else
		
#endif
	}
	
	FORCEINLINE static Vector3f Sub(const Vector3f& vec1, float c)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_sub_ps(_mm_load_ps((float*)&vec1), _mm_set1_ps(c)));
#else
		
#endif
	}
	
	FORCEINLINE static Vector3f Mul(const Vector3f& vec1, float c)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_mul_ps(_mm_load_ps((float*)&vec1), _mm_set1_ps(c)));
#else
		
#endif
	}
	
	FORCEINLINE static Vector3f Div(const Vector3f& vec1, float c)
	{
#ifdef _SIMD_VECTORS
		Vector3f rvec;
		_mm_store_ps((float*)&rvec, _mm_div_ps(_mm_load_ps((float*)&vec1), _mm_set1_ps(c)));
#else
		
#endif
	}
public:
	
	FORCEINLINE Vector3f operator*(float c) const
	{
		return Vector3f::Mul(*this, c);
	}
	
	FORCEINLINE Vector3f operator/(float c) const
	{
		return Vector3f::Div(*this, c);
	}
	
	FORCEINLINE Vector3f operator+(float c) const
	{
		return Vector3f::Add(*this, c);
	}
	
	FORCEINLINE Vector3f operator-(float c) const
	{
		return Vector3f::Sub(*this, c);
	}
	
	FORCEINLINE void operator*=(float c)
	{
		this->Mul(c);
	}
	
	FORCEINLINE void operator/=(float c)
	{
		this->Div(c);
	}
	
	FORCEINLINE Vector3f operator+(const Vector3f& vec) const
	{
		return Vector3f::Add(*this, vec);
	}
	
	FORCEINLINE Vector3f operator-(const Vector3f& vec) const
	{
		return Vector3f::Sub(*this, vec);
	}
	
	FORCEINLINE void operator+=(const Vector3f& vec)
	{
		this->Add(vec);
	}
	
	FORCEINLINE void operator-=(const Vector3f& vec)
	{
		this->Sub(vec);
	}
} _VECTOR_PACKED_CLASS;


class _VECTOR_ALIGN Vector3d
{
public:
	double x, y, z;
	
#ifdef _SIMD_VECTORS
	double dummy = 1; // Padding again
#endif

public:
	
	Vector3d()
	{
		this->x = 0.0;
		this->y = 0.0;
		this->z = 0.0;
	}
	
	Vector3d(double x, double y, double z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	
	Vector3d(const Vector3d& vec)
	{
		this->x = vec.x;
		this->y = vec.y;
		this->z = vec.z;
	}
	
	Vector3d(Vector3d&& vec)
	{
		this->x = vec.x; vec.x = 0.0;
		this->y = vec.y; vec.y = 0.0;
		this->z = vec.z; vec.z = 0.0;
	}
	
public:
	
	/*
	 * Normalize the vectorin place and return magnitude
	 */
	FORCEINLINE double Normalize()
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	/*
	 * Dot product of two vectors
	 */
	FORCEINLINE double DotProduct(const Vector3d& vec) const
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	/*
	 * Cross product of two vectors
	 */ 
	FORCEINLINE Vector3d CrossProduct(const Vector3d& vec) const
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
public:
	FORCEINLINE void Mul(double c)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	FORCEINLINE void Div(double c)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	FORCEINLINE void Add(double c)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	FORCEINLINE void Sub(double c)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}

public:
	FORCEINLINE void Add(const Vector3d& vec)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
	FORCEINLINE void Sub(const Vector3d& vec)
	{
#ifdef _SIMD_VECTORS
		
#else
		
#endif
	}
	
} _VECTOR_PACKED_CLASS;

#ifdef _WINDOWS
#pragma pop
#endif 
