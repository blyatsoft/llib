/*

Vector4.h

4 component vector stuff

*/
#pragma once

#include "Vector3.h"
#include "../LLib.h"

#include <math.h>

#ifdef _SIMD_VECTORS
#include <xmmintrin.h>
#include <intrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#endif

//////////////////////////
template<class I>
class TVector3;
//////////////////////////

template<class I>
class _VECTOR_ALIGN TVector4
{
public:
	I x, y, z, m;

public:
	TVector4(I x, I y, I z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->Normalize();
	}

	TVector4(I x, I y, I z, I m)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->m = m;
	}
	
	TVector4(const TVector4<I>& other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->m = other.m;
	}
	
	TVector4(TVector4<I>&& other)
	{
		this->x = other.x;
		other.x = 0;
		this->y = other.y;
		other.y = 0;
		this->z = other.z;
		other.z = 0;
		this->m = other.m;
		other.m = 0;
	}
	
public:
	
	FORCEINLINE I GetX() const
	{
		return this->x;
	}
	
	FORCEINLINE I GetY() const
	{
		return this->y;
	}
	
	FORCEINLINE I GetZ() const
	{
		return this->z;
	}
	
	FORCEINLINE I GetM() const
	{
		return this->m;
	}
	
	FORCEINLINE void SetX(I x)
	{
		this->x = x;
	}
	
	FORCEINLINE void SetY(I y)
	{
		this->y = y;
	}
	
	FORCEINLINE void SetZ(I z)
	{
		this->z = z;
	}

private:

public:
	
	FORCEINLINE void Normalize()
	{
		I mag = sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
		this->m = mag;
		this->x /= m;
		this->y /= m;
		this->z /= m;
	}
	
	FORCEINLINE TVector3<I> Denormalize() const
	{
		return TVector3<I>(this->x * m, this->y * m, this->z * m);
	}
	
	FORCEINLINE I Distance(const TVector4<I>& other) const
	{
		return this->Distance(Denormalize());
	}
	
	FORCEINLINE I Distance(const TVector3<I>& other) const
	{
		return (I)sqrt((double)(((x - other.x) * (x - other.x)) + ((y - other.y)*(y - other.y)) + ((z - other.z)*(z - other.z))));
	}

	FORCEINLINE I Dot(const TVector4<I>& other) const
	{
		return (I)((this->x*this->m)*(other.x*other.m) + (this->y*this->m)*(other.y*other.m) + (this->z*this->m)*(other.z*other.m));
	}

	FORCEINLINE I Dot(const TVector3<I>& other) const
	{
		return (I)(this->x * other.x + this->y * other.y + this->z * other.z);
	}
	

} _VECTOR_PACKED_CLASS;
