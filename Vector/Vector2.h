#pragma once

#include "../LLib.h"

#include <math.h>

////////////////////////////////
// Forward declarations
template<class I>
class TVector2;

template<class I>
class TVector3;
////////////////////////////////

//Helper macros for TVector2
#define OPERATOR_MUL(type) TVector2<I>& operator*(type other) const { return TVector2<I>(other*x, other*y); }
#define OPERATOR_DIV(type) TVector2<I>& operator/(type other) const { return TVector2<I>(x/other, y/other); }

#ifdef _WINDOWS
#pragma pack(push, 1)
#endif

template<class I>
class _VECTOR_ALIGN TVector2
{
private:

public:

	I x, y;

	TVector2()
	{
		x = (I)0;
		y = (I)0;
	}

	TVector2(const TVector2<I>& other)
	{
		x = other.x;
		y = other.y;
	}

	TVector2(I x, I y)
	{
		this->x = x;
		this->y = y;
	}
	
	I GetX() const { return x; }

	I GetY() const { return y; }

	void SetX(I x) { this->x = x; }

	void SetY(I y) { this->y = y; }

	#if 0

	TVector2<I>& operator+(const TVector2<I>& other) const
	{
		return TVector2<I>(x + other.x, y + other.y);
	}

	TVector2<I>& operator-(const TVector2<I>& other) const
	{
		return TVector2<I>(x - other.x, y - other.y);
	}

	void operator+=(const TVector2<I>& other)
	{
		x += other.x;
		y += other.y;
	}

	void operator-=(const TVector2<I>& other)
	{
		x -= other.x;
		y -= other.y;
	}

	bool operator==(const TVector2<I>& other) const 
	{
		return (x == other.x && y == other.y);
	}

	bool operator!=(const TVector2<I>& other) const
	{
		return (x != other.x && y != other.y);
	}

	#endif

	I Distance(const TVector2<I>& other) const
	{
		return (I)sqrt((double)((x - other.x) * (x - other.x) - (y - other.y) * (y - other.y)));
	}

	TVector2<I> Midpoint(const TVector2<I>& other) const
	{
		return TVector2<I>((this->x + other.x) / 2, (this->y + other.y) / 2);
	}

	TVector3<I> Normalize() const
	{
		TVector3<I> norm;
		I mag = Magnitude();

		norm.x = this->x / mag;
		norm.y = this->y / mag;
		norm.z = mag;

		return norm;
	}

	I Dot(const TVector2<I>& other) const
	{
		return (I)(x*other.x + y * other.y);
	}

	I Magnitude() const
	{
		return (I)sqrt(x * x + y * y);
	}

	TVector2<I> Lerp(const TVector2<I>& other, float bias)
	{
		return (*this) * bias + other * (1.0 - bias);
	}
} _VECTOR_PACKED_CLASS;

#ifdef _WINDOWS
#pragma pop
#endif

#undef OPERATOR_MUL
#undef OPERATOR_DIV
