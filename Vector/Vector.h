/*

Vector.h

Main header for vector types

Notes:

Unlike Vector2 and Vector3, Vector4 is aligned on a 16 byte boundry. This is to allow SIMD operations to be performed on it.

All vectors are packed types

*/

#pragma once

#include "LLib.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

//TODO: FIX THESE OPERATORS

///////////////////////////////////
// Forward declarations
template<class I>
class TVector2;
template<class I>
class TVector3;
template<class I>
class TVector4;
///////////////////////////////////

///////////////////////////////////
// Common TVector2 specializations
typedef TVector2<float> Vector2;
typedef TVector2<double> Vector2d;
typedef TVector2<int> Vector2i;
///////////////////////////////////

///////////////////////////////////
// Common TVector3 specializations
typedef TVector3<float> Vector3;
typedef TVector3<double> Vector3d;
typedef TVector3<int> Vector3i;
///////////////////////////////////

///////////////////////////////////
// Common TVector4 specializations
typedef TVector4<float> Vector4;
typedef TVector4<double> Vector4d;
typedef TVector4<int> Vector4i;
///////////////////////////////////

#ifdef _SIMD_VECTORS
#include <xmmintrin.h>
#include <intrin.h>
#endif

/*


Vector2<Float> Operators


*/
FORCEINLINE TVector2<float> operator+(const TVector2<float>& vec1, const TVector2<float>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>(vec1.x + vec2.x, vec1.y + vec2.y);
#else
	return TVector2<float>(vec1.x + vec2.x, vec1.y + vec2.y);
#endif
}

FORCEINLINE TVector2<float> operator-(const TVector2<float>& vec1, const TVector2<float>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>(vec1.x - vec2.x, vec1.y - vec2.y);
#else
	return TVector2<float>(vec1.x - vec2.x, vec1.y - vec2.y);
#endif
}

FORCEINLINE TVector2<float> operator*(const TVector2<float>& vec1, float constant)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>(vec1.x * constant, vec1.y * constant);
#else
	return TVector2<float>(vec1.x * constant, vec1.y * constant);
#endif
}

FORCEINLINE TVector2<float> operator*(const TVector2<float>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>((float)(vec1.x * constant), (float)(vec1.y * constant));
#else
	return TVector2<float>((float)(vec1.x * constant), (float)(vec1.y * constant));
#endif
}

FORCEINLINE TVector2<float> operator/(const TVector2<float>& vec1, float constant)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>(vec1.x / constant, vec1.y / constant);
#else
	return TVector2<float>(vec1.x / constant, vec1.y / constant);
#endif
}

FORCEINLINE TVector2<float> operator/(const TVector2<float>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector2<float>((float)(vec1.x / constant), (float)(vec1.y / constant));

#else
	return TVector2<float>((float)(vec1.x / constant), (float)(vec1.y / constant));
#endif
}

FORCEINLINE bool operator==(const TVector2<float>& vec1, const TVector2<float>& vec2)
{
	return (vec1.x == vec2.x && vec1.y == vec2.y);
}

FORCEINLINE bool operator!=(const TVector2<float>& vec1, const TVector2<float>& vec2)
{
	return (vec1.x != vec2.x || vec1.y != vec2.y);
}

FORCEINLINE TVector2<float> operator-(const TVector2<float>& vec1)
{
	return TVector2<float>(-vec1.x, -vec1.y);
}

FORCEINLINE TVector2<float> operator+(const TVector2<float>& vec1)
{
	return TVector2<float>(+vec1.x, +vec1.y);
}

/*


Vector2<double> Operators


*/
FORCEINLINE TVector2<double> operator+(TVector2<double> vec1, TVector2<double> vec2)
{
#ifdef _SIMD_VECTORS
	__m128 v1 = *reinterpret_cast<__m128*>(&vec1);
	__m128 v2 = *reinterpret_cast<__m128*>(&vec2);

	__m128 dst = _mm_add_pd(v1, v2);

	double* res = reinterpret_cast<double*>(&dst);

	return TVector2<double>(res[0], res[1]);
#else
	return TVector2<double>(vec1.x + vec2.x, vec1.y + vec2.y);
#endif
}

FORCEINLINE TVector2<double> operator-(TVector2<double> vec1, TVector2<double> vec2)
{
#ifdef _SIMD_VECTORS
	__m128 v1 = *reinterpret_cast<__m128*>(&vec1);
	__m128 v2 = *reinterpret_cast<__m128*>(&vec2);

	__m128 dst = _mm_sub_pd(v1, v2);

	double* res = reinterpret_cast<double*>(&dst);

	return TVector2<double>(res[0], res[1]);
#else
	return TVector2<double>(vec1.x - vec2.x, vec1.y - vec2.y);
#endif
}

FORCEINLINE TVector2<double> operator*(TVector2<double> vec1, double constant)
{
#ifdef _SIMD_VECTORS

	__m128d v1 = *reinterpret_cast<__m128*>(&vec1);
	__m128d v2 = _mm_set_pd1(constant);

	__m128d dst = _mm_mul_pd(v1, v2);

	double* res = reinterpret_cast<double*>(&dst);

	return TVector2<double>(res[0], res[1]);
#else
	return TVector2<double>(vec1.x * constant, vec1.y * constant);
#endif
}

FORCEINLINE TVector2<double> operator*(TVector2<double> vec1, float constant)
{
#ifdef _SIMD_VECTORS
	__m128 v1 = *reinterpret_cast<__m128*>(&vec1);
	__m128 v2 = _mm_set_pd1((double)constant);

	__m128 dst = _mm_mul_pd(v1, v2);

	double* res = reinterpret_cast<double*>(&dst);

	return TVector2<double>(res[0], res[1]);
#else
	return TVector2<double>(vec1.x * constant, vec1.y * constant);
#endif
}

FORCEINLINE TVector2<double> operator/(TVector2<double> vec1, double constant)
{
#ifdef _SIMD_VECTORS
	__m128 v1 = *reinterpret_cast<__m128*>(&vec1);
	__m128 v2 = _mm_set_pd1((double)constant);

	__m128 dst = _mm_div_pd(v1, v2);

	double* res = reinterpret_cast<double*>(&dst);

	return TVector2<double>(res[0], res[1]);
#else
	return TVector2<double>(vec1.x / constant, vec1.y / constant);
#endif
}

FORCEINLINE bool operator==(const TVector2<double>& vec1, const TVector2<double>& vec2)
{
	return (vec1.x == vec2.x && vec1.y == vec2.y);
}

FORCEINLINE bool operator!=(const TVector2<double>& vec1, const TVector2<double>& vec2)
{
	return (vec1.x != vec2.x || vec1.y != vec2.y);
}

FORCEINLINE TVector2<double> operator-(const TVector2<double>& vec1)
{
	return TVector2<double>(-vec1.x, -vec1.y);
}

FORCEINLINE TVector2<double> operator+(const TVector2<double>& vec1)
{
	return TVector2<double>(+vec1.x, +vec1.y);
}

/*


Vector3<float> Operators


*/
FORCEINLINE TVector3<float> operator+(const TVector3<float>& vec1, const TVector3<float>& vec2)
{
#ifdef _SIMD_VECTORS
	//return TVector3<float>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
	__m128 v1 = _mm_load_ps(reinterpret_cast<const float*>(&vec1));
	__m128 v2 = _mm_load_ps(reinterpret_cast<const float*>(&vec2));
	__m128 res = _mm_add_ps(v1,v2);
	return *reinterpret_cast<TVector3<float>*>(&res);
#else
	return TVector3<float>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
#endif
}

FORCEINLINE TVector3<float> operator-(const TVector3<float>& vec1, const TVector3<float>& vec2)
{
#ifdef _SIMD_VECTORS
	__m128 v1 = _mm_load_ps(reinterpret_cast<const float*>(&vec1));
	__m128 v2 = _mm_load_ps(reinterpret_cast<const float*>(&vec2));
	__m128 res = _mm_sub_ps(v1,v2);
	return *reinterpret_cast<TVector3<float>*>(&res);
#else
	return TVector3<float>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
#endif
}

FORCEINLINE TVector3<float> operator*(const TVector3<float>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<float>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#else
	return TVector3<float>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector3<float> operator*(const TVector3<float>& vec1, float constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<float>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#else
	return TVector3<float>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector3<float> operator/(const TVector3<float>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<float>(vec1.x / constant, vec1.y / constant, vec1.z / constant);
#else
	return TVector3<float>(vec1.x / constant, vec1.y / constant, vec1.z / constant);
#endif
}

FORCEINLINE bool operator==(const TVector3<float>& vec1, const TVector3<float>& vec2)
{
	return (vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z);
}

FORCEINLINE bool operator!=(const TVector3<float>& vec1, const TVector3<float>& vec2)
{
	return (vec1.x != vec2.x || vec1.y != vec2.y || vec1.z != vec2.z);
}

FORCEINLINE TVector3<float> operator-(const TVector3<float>& vec1)
{
	return TVector3<float>(-vec1.x, -vec1.y, -vec1.z);
}

FORCEINLINE TVector3<float> operator+(const TVector3<float>& vec1)
{
	return TVector3<float>(+vec1.x, +vec1.y, +vec1.z);
}


/*


Vector3<double> Operators


*/
FORCEINLINE TVector3<double> operator+(const TVector3<double>& vec1, const TVector3<double>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector3<double>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
#else
	return TVector3<double>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
#endif
}

FORCEINLINE TVector3<double> operator-(const TVector3<double>& vec1, const TVector3<double>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector3<double>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
#else
	return TVector3<double>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
#endif
}

FORCEINLINE TVector3<double> operator*(const TVector3<double>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#else
	return TVector3<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector3<double> operator*(const TVector3<double>& vec1, float constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#else
	return TVector3<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector3<double> operator/(const TVector3<double>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector3<double>(vec1.x / constant, vec1.y / constant, vec1.z / constant);
#else
	return TVector3<double>(vec1.x / constant, vec1.y / constant, vec1.z / constant);
#endif
}

FORCEINLINE bool operator==(const TVector3<double> vec1, const TVector3<double> vec2)
{
	return (vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z);
}

FORCEINLINE bool operator!=(const TVector3<double> vec1, const TVector3<double> vec2)
{
	return (vec1.x != vec2.x || vec1.y != vec2.y || vec1.z != vec2.z);
}

FORCEINLINE TVector3<double> operator-(const TVector3<double> vec1)
{
	return TVector3<double>(-vec1.x, -vec1.y, -vec1.z);
}

FORCEINLINE TVector3<double> operator+(const TVector3<double> vec1)
{
	return TVector3<double>(+vec1.x, +vec1.y, +vec1.z);
}

/*


Vector4 Double Specialization SIMD Operators


*/
FORCEINLINE TVector4<double> operator+(const TVector4<double>& vec1, const TVector4<double>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector4<double>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
#else
	return TVector4<double>(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
#endif
}

FORCEINLINE TVector4<double> operator-(const TVector4<double>& vec1, const TVector4<double>& vec2)
{
#ifdef _SIMD_VECTORS
	return TVector4<double>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
#else
	return TVector4<double>(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
#endif
}

FORCEINLINE TVector4<double> operator*(const TVector4<double>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector4<double>(vec1.x, vec1.y, vec1.z, vec1.m * constant);
#else
	return TVector4<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector4<double> operator*(const TVector4<double>& vec1, float constant)
{
#ifdef _SIMD_VECTORS
	return TVector4<double>(vec1.x, vec1.y, vec1.z, vec1.m * constant);
#else
	return TVector4<double>(vec1.x * constant, vec1.y * constant, vec1.z * constant);
#endif
}

FORCEINLINE TVector4<double> operator/(const TVector4<double>& vec1, double constant)
{
#ifdef _SIMD_VECTORS
	return TVector4<double>(vec1.x, vec1.y, vec1.z, vec1.m / constant);
#else
	return TVector4<double>(vec1.x / constant, vec1.y / constant, vec1.z / constant);
#endif
}

FORCEINLINE bool operator==(const TVector4<double> vec1, const TVector4<double> vec2)
{
	return (vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z && vec1.m == vec2.m);
}

FORCEINLINE bool operator!=(const TVector4<double> vec1, const TVector4<double> vec2)
{
	return (vec1.x != vec2.x || vec1.y != vec2.y || vec1.z != vec2.z || vec1.m != vec2.m);
}

FORCEINLINE TVector4<double> operator-(const TVector4<double> vec1)
{
	return TVector4<double>(-vec1.x, -vec1.y, -vec1.z, vec1.m);
}

FORCEINLINE TVector4<double> operator+(const TVector4<double> vec1)
{
	return TVector4<double>(+vec1.x, +vec1.y, +vec1.z, vec1.m);
}

/*


Vector4 Double Specialization SIMD Operators


*/
