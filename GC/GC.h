/*

GC.h

Experimental C++ garbage collector.

*/
#pragma once

#include "LLib.h"

#include "Containers/Pointer.h"
#include "Containers/Array.h"

LLIB_HEADER_START

/*

GarbageCollector

This is essentially a memory manager.
You can allocate new pointers using the AllocateManagedObject(num) function, which returns a ReferencePointer to the managed object.
When all subsequent references to the object are destroyed, the object will be deallocated during a GC pass. This can be done using other methods too.

*/
class LLIB_API GarbageCollector 
{
private:

	template<class T>
	friend class ReferencePointer;



private:


public:

	GarbageCollector(size_t init_block = 1000)
	{

	}

	template<class T>
	T* AllocateManagedObject(size_t num);

	/*
	Allocates a shared reference to the specified object.
	*/
	template<class T, class... Args>
	SharedReference<T> AllocateObject(Args... args);

	void* AllocateManagedMemory(size_t length);
};

LLIB_HEADER_END