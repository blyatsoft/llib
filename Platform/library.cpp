#include "library.h"

#undef LoadLibrary

#ifdef _POSIX
#include <dlfcn.h>
#endif

LLIB_SOURCEFILE

LibraryInfo * LoadLibrary(const char * name)
{
	LibraryInfo* info = new LibraryInfo();
	
#ifdef WINDOWS
	HANDLE hdll = LoadLibraryA(name);
	if (hdll != NULL)
	{
		info->Path = name;
		info->Handle = (void*)hdll;
	}
	else
		return NULL;
#elif defined(_POSIX)
	void* hdll = dlopen(name, RTLD_GLOBAL);
	if(hdll)
	{
		info->Path = name;
		info->Handle = hdll;
	}
	else
		return NULL;
#endif
	return info;
}

void * GetProcAddress(void * dl, const char * name)
{
	assert(dl != NULL && name != NULL);
#ifdef WINDOWS
	return (void*)GetProcAddress((HMODULE)dl, (LPCSTR)name);
#elif defined(_POSIX)
	return dlsym(dl, name);
#endif
}

void * GetProcAddress(const LibraryInfo * dl, const char * name)
{
	assert(dl->Handle != NULL);
#ifdef WINDOWS
	return (void*)GetProcAddress((HMODULE)dl->Handle, (LPCSTR)name);
#elif defined(_POSIX)
	return dlsym(dl->Handle, name);
#endif
}

void FreeLibrary(LibraryInfo * dl)
{
#ifdef WINDOWS
	FreeLibrary((HMODULE)dl->Handle);
#elif defined(_POSIX)
	dlclose(dl->Handle);
#endif 
}

void FreeLibrary(void * dl)
{
#ifdef WINDOWS
	FreeLibrary((HMODULE)dl);
#elif defined(_POSIX)
	dlclose(dl);
#endif
}
