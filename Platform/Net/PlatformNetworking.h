/*


PlatformNetworking.h


Platform abstracted networking implementation


*/
#pragma once

#include "LLib.h"
#include "Containers/Buffer.h"
#include "ipdef.h"

#ifdef _WINDOWS
#include <WinSock2.h>
#include <WS2tcpip.h>
#endif

//Standard includes
#include <memory>
#include <functional>

LLIB_HEADER_START

/*

__internal_* classes are provided so that the main networking classes have sizes independent on their platform.

*/

class NetworkingService;


struct LLIB_API SocketAddress
{
private:
	uint8 AddressData[16];
	uint16 AddressFamily;

#ifdef _WINDOWS
	//Windows only constructors for working with sockets
	SocketAddress(const sockaddr& other);
	SocketAddress(const sockaddr* other);
	
	void LoadSockaddr(const sockaddr& other)
	{
		AddressFamily = other.sa_family;
		memcpy(AddressData, other.sa_data, 16);
	}
#endif

public:
	SocketAddress();

};

struct __internal_socket_data
{
#ifdef _WINDOWS
	SOCKET m_socket;
#else

#endif
};

struct LLIB_API Socket
{
public:
	enum class ESocketDomain
	{
		Unspecified = 0,
		INet,				/* IPv4 */
		IPv4 = INet,		/* IPv4 */
		INet6,				/* IPv6 */
		IPv6 = INet6,		/* IPv6*/
		IPX,				/* IPX */
		AppleTalk,			/* AppleTalk */
		NetBIOS,			/* NetBIOS */
		Bluetooth,			/* Bluetooth */
	};

	enum class ESocketType
	{
		Unspecified = 0,
		Stream,				/* Stream type transfer protocol */
		Datagram,			/* Datagram */
		DGram = Datagram,	/* Datagram */
		Raw,
		RDM,
		ReliableDatagram = RDM,
		SeqPacket,
		SequentialPacket = SeqPacket,
	};

	enum class ESocketProtocol
	{
		Unspecified = 0,
		ICMP,
		IGMP,
		RFCOMM,
		TCP,
		UDP,
		ICMPV6,
		RM,
	};

	enum class ESocketFlags : uint32_t
	{
		Good = 1 << 0,					/* Socket is valid */
		Failed = 1 << 1,				/* Socket isn't valid for unspecified reason */
		AccessDenied = 1 << 2,			/* Insufficient perms */
		AddrNotSupported = 1 << 3,		/* Address not supported for this socket type */
		ProtoNotSupported = 1 << 4,		/* Protocol not supported */
		BadProtocol = 1 << 5,			/* Invalid protocol type */
		OutOfMemory = 1 << 6,			/* Not enough memory to create socket descriptor */
		NoSocketDescriptors = 1 << 7,	/* No remaining socket descriptors*/
	};

	enum class ESocketError : uint32_t
	{
		OK =					0 << 0,		/* Socket is valid */
		Failed =				1 << 0,		/* Socket isn't valid for unspecified reason */
		AccessDenied =			1 << 1,		/* Insufficient perms */
		AddrNotSupported =		1 << 2,		/* Address not supported for this socket type */
		ProtoNotSupported =		1 << 3,		/* Protocol not supported */
		BadProtocol =			1 << 4,		/* Invalid protocol type */
		OutOfMemory =			1 << 5,		/* Not enough memory to create socket descriptor */
		NoSocketDescriptors =	1 << 6,		/* No remaining socket descriptors*/
		SocketAlreadyConnected= 1 << 7,		/* Socket is already connected */
		SocketClosed =			1 << 8,		/* Socket is closed */
		SocketConnecting =		1 << 9,		/* A non-blocking Connect call is already underway */
		SocketListening =		1 << 10,	/* Socket is already listening */
		SocketAccepting =		1 << 11,	/* Socket is already accepting connections */
		MalformedParameter =	1 << 12,	/* An invalid parameter was passed */
		NoNetworkingService =	1 << 13,	/* The networking service isn't initialized */
		Uninitialized =			NoNetworkingService, /* Same as ladder */
		AddrInUse =				1 << 14,	/* Specified address is already in use */
		CallInProgress =		1 << 15,	/* Some type of nonblocking call is in progress */
	};

private:
	NetworkingService * NetService;

	uint32_t m_SocketFlags;

	ESocketProtocol m_SocketProtocol;

	ESocketType m_SocketType;

	ESocketDomain m_SocketDomain;

	__internal_socket_data* m_socket_data;

public:
	/* getters */

	FORCEINLINE ESocketProtocol GetSocketProtocol() const { return m_SocketProtocol; }

	FORCEINLINE ESocketType GetSocketType() const { return m_SocketType; }

	FORCEINLINE ESocketDomain GetSocketDomain() const { return m_SocketDomain; }

public:
	/*
	
	bool delegate(Socket* this_socket, SocketAddress incoming_addr);

	Called when a connection attempt is made to this socket. This will only be called when
	the socket is in listening mode.

	incoming_addr is the address of the socket trying to connect to this one

	this_socket is a pointer to this socket class.

	return true to accept the connection request, return false to deny

	*/
	std::function<bool(Socket*, SocketAddress)> OnIncomingRequestDelegate;

	/*
	
	void delegate(ESocketError err);

	Called when a socket error occurs

	*/
	std::function<void(ESocketFlags)> OnSocketErrorDelegate;

	/*

	void delegate();

	Called when the socket is closed.

	*/
	std::function<void()> OnSocketClosedDelegate;

public:
	Socket(NetworkingService* service, ESocketDomain domain, ESocketType type, ESocketProtocol protocol);
	~Socket();
private:
	/*

	Socket* CreateSocket(NetworkingService* service, ESocketDomain domain, ESocketType type, ESocketProtocol);

	Desc:
		Creates a new socket with the specified networking service.

	Parameters:
		service		- The networking service to use. Cannot be NULL
		domain		- Socket domain
		type		- Socket type
		protocol	- Socket protocol

	Returns:
		The new socket or nullptr if the function call failed.

	*/
	Socket * CreateSocket(NetworkingService* service, ESocketDomain domain, ESocketType type, ESocketProtocol protocol);

	/*
	
	uint32_t Listen(uint16_t backlog = 256);

	Desc:
		Puts this socket into a listening state with the specified backlog size.

	Parameters:
		backlog		- Backlog size to hold incoming connections. Should be between 200 and 65535

	Returns:
		Error Codes

	*/
	uint32_t Listen(uint16_t baclog = 256);

	/*
	
	uint32_t Accept(SocketAddress& addr);

	Desc:
		Synchronously accepts a connection. This will block the caller and wait for a new request
		if a pending is not available in the connection queue.

	Parameters:
		addr	- Variable to store the address of the accepted connection in

	Returns:
		Error Codes

	*/
	uint32_t Accept(SocketAddress& addr);

	/*
	
	uint32_t AcceptAsync(std::function<void(SocketAddress)> callback);

	Desc:
		Asynchronously accepts a connection on this socket. This will not block the caller
		even if a connection isn't immediately available.

	Parameters:
		callback	- The delegate to be called when a connection is accepted

	Returns:
		Error Codes

	Delegate structure:
		void delegate(uint32_t errors, SocketAddress accepted_addr);

	*/
	uint32_t AcceptAsync(std::function<void(uint32_t, SocketAddress)> callback);

	uint32_t Connect(SocketAddress addr);

	uint32_t ConnectAsync(SocketAddress addr, std::function<void(uint32_t, SocketAddress)> callback);

	/*

	uint8_t Bind(SocketAddress addr);

	Desc:
		Binds a socket address to this socket

	Parameters:
		SocketAddresss addr		-	The address to bind to

	Returns:
		Error flags

	*/
	uint8_t Bind(SocketAddress addr);

	/*

	uint32_t Send(const char* buffer, uint32_t length, uint32_t flags);

	Desc:
		Synchronously sends a buffer of data over this socket.
		Will fail if the socket is not bound or if a parameter is invalid.

	Parameters:
		const char* buffer	-	The buffer to send, cannot be NULL
		uint32_t length		-	The length of the buffer, cannot be 0
		uint32_t flags		-	Flags

	Returns:
		Error flags

	*/
	uint32_t Send(const char* buffer, uint32_t length, uint32_t flags);

	/*

	uint32_t Send(const ByteBuffer& buffer, uint32_t flags);

	Desc:
		Sends the data inside of the ByteBuffer over the socket.
		Will fail if the socket is not bound or if a parameter is invalid.

	Parameters:
		ByteBuffer buffer	-	The buffer to send, must be valid
		uint32_t flags		-	Flags

	Returns:
		Error flags
	
	*/
	uint32_t Send(const ByteBuffer& buffer, uint32_t flags);

	/*

	uint32_t SendAsync(const char* buffer, uint32_t length, uint32_t flags, std::function<void(uint32_t)> callback);

	Desc:
		Asynchronously sends a buffer of data over the socket.
		Will fail if the socket is not bound or if a parameter is invalid

	Parameters:
		buffer		-	Buffer to send, cannot be NULL
		length		-	Length of buffer to send, cannot be 0
		flags		-	Flags
		callback	-	Callback to call after the sending is complete

	Returns:
		Error Flags

	Delegate structure:
		void delegate(uint32_t errors);

	*/
	uint32_t SendAsync(const char* buffer, uint32_t length, uint32_t flags, std::function<void(uint32_t)> callback);

	/*

	uint32_t SendAsync(const ByteBuffer& buffer, uint32_t flags, std::function<void(uint32_t)> callback);

	Desc:
		Asynchronously sends a buffer of data over the socket
		Will fail if the socket is not bound or if a parameter is invalid

	Parameters:
		buffer		-	Buffer to send
		flags		-	Flags
		callback	-	Function to be called after hte sending is complete

	Returns:
		Error flags

	Delegate structure:
		void delegate(uint32_t errors);

	*/
	uint32_t SendAsync(const ByteBuffer& buffer, uint32_t flags, std::function<void(uint32_t)> callback);

	/*

	uint32_t Receive(char* buffer, uint32_t buf_len, uint32& received, uint32_t flags);

	Desc:
		Waits for data to be received on the socket. This is a blocking call.
		Will fail if the socket is not bound or if a parameter is invalid.

	Parameters:
		buffer		-	Buffer to put data into
		buf_len		-	Length of the buffer
		received	-	Total actual received bytes
		flags		-	Flags

	Returns:
		Error Flags

	*/
	uint32_t Receive(char* buffer, uint32_t buf_len, uint32_t& received, uint32_t flags);

	/*

	uint32_t Receive(ByteBuffer& buf, uint32_t flags);

	Desc:
		Waits for data to be received on the socket then reads the data into the specified buffer.
		Will fail if the socket is not bout or if a parameter is invalid

	Parameters:
		buffer	-	The buffer to hold the data
		flags	-	Flags

	Returns:
		Error Flags
	
	*/
	uint32_t Receive(ByteBuffer& buf, uint32_t flags);

	/*

	uint32_t ReceiveAsync(char* buffer, uint32_t buf_len, uint32_t flags, std::function<void(char*, uint32_t, uint32_t)> callback);

	Desc:
		Asynchronously receives data over the socket and stores it in a buffer. Calls a callback when the operation is completed.
		Will fail if the socket is not bound or if a parameter is invalid.

	Parameters:
		buffer		- Pointer to the buffers
		buf_len		- Length of the buffer
		flags		- Flags
		callback	- Delegate to be called when the operation is completed

	Returns:
		Error Flags

	Delegate structure:
		void delegate(char* buffer, uint32_t bytes_received, uint32_t errors);

	*/
	uint32_t ReceiveAsync(char* buffer, uint32_t buf_len, uint32_t flags, std::function<void(char*, uint32_t, uint32_t)> callback);

	/*

	uint32_t ReceiveAsync(uint32_t flags, std::function<void(ByteBuffer, uint32_t)> callback);

	Desc:
		Asnychronously receives data over the socket and stores it in a buffer. Calls a callback when the operation is completed.
		Will fail if the socket is not bound or if a parameter is invalid.

	Parameters:
		flags		- Flags
		callback	- Delegate to be called when the operation is completed

	Returns:
		Error Flags

	Delegate structure:
		void delegate(ByteBuffer buffer, uint32_t errors);

	*/
	uint32_t ReceiveAsync(uint32_t flags, std::function<void(ByteBuffer, uint32_t)> callback);

	/*
	
	uint32_t Close();

	Desc:
		Closes this socket.

	Returns:
		Error Codes

	*/
	uint32_t Close();

	/*
	
	uint32_t Disconnect();

	Desc:
		Disconnects this socket

	Parameters:
		op	- Operation to perform. 0 to disable receives, 1 to disable sending and 2 to disable both

	Returns:
		Error codes

	*/
	uint32_t Disconnect(uint32_t op = 2);
};
class LLIB_API NetworkingService
{
	struct
	{
	#ifdef _WINDOWS
		WSADATA WsaData;
	#endif
	} __internal_data;

public:
	NetworkingService();

	~NetworkingService();

public:
	static NetworkingService* StartNetworkingService();

	void Shutdown();

	Socket* CreateSocket(Socket::ESocketDomain domain, Socket::ESocketType type, Socket::ESocketProtocol protocol);
};



LLIB_HEADER_END