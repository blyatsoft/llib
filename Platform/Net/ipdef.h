/*

ipdef.h

Internet protocol defs for use with the networking and sockets library

*/
#pragma once

#include "LLib.h"

LLIB_HEADER_START

struct LLIB_API IPv4Address
{
public:
	uint32_t m_data = 0;

private:
	/* Private conversion constructors for internal use only */
	static uint32_t _FromString(const char* str);
public:
	IPv4Address(const IPv4Address& other)
	{
		this->m_data = other.m_data;
		this->m_data = other.m_data;
	}

	IPv4Address(uint32_t data) { m_data = data; };

	IPv4Address(const char* addr);

	IPv4Address() { m_data = 0; };

	IPv4Address(char a, char b, char c, char d)
	{
		char* arr = (char*)&m_data;
		arr[0] = a;
		arr[1] = b;
		arr[2] = c;
		arr[4] = d;
	}

	IPv4Address(uint16_t a, uint16_t b)
	{
		uint16_t* arr = (uint16_t*)&m_data;
		arr[0] = a;
		arr[1] = b;
	}

public:
	const char* ToString() const;

	static IPv4Address& FromString(const char* str)
	{
		IPv4Address addr = IPv4Address(IPv4Address::_FromString(str));
		return addr;
	};

public:
	FORCEINLINE IPv4Address& operator=(uint32_t data)
	{
		m_data = data;
		return *this;
	}

	FORCEINLINE IPv4Address& operator=(const IPv4Address& other)
	{
		m_data = other.m_data;
		return *this;
	}

	FORCEINLINE IPv4Address& operator=(IPv4Address&& other)
	{
		m_data = other.m_data;
		other.m_data = 0;
		return *this;
	}

	FORCEINLINE IPv4Address& operator=(const char* str)
	{
		m_data = _FromString(str);
		return *this;
	}

	FORCEINLINE bool operator==(const IPv4Address& other) const
	{
		return m_data == other.m_data;
	}

	FORCEINLINE bool operator==(uint32_t other) const
	{
		return m_data == other;
	}

	FORCEINLINE bool operator==(const char* other) const
	{
		return m_data == _FromString(other);
	}

	FORCEINLINE bool operator!=(const IPv4Address& other) const
	{
		return m_data != other.m_data;
	}

	FORCEINLINE bool operator!=(uint32_t other) const
	{
		return m_data != other;
	}

	FORCEINLINE bool operator!=(const char* other) const
	{
		return m_data != _FromString(other);
	}

};

//IPv4 Aliases
typedef IPv4Address IPv4;
typedef IPv4Address IPAddress;
typedef IPv4Address INetAddress;

struct IPv6Address
{
	
};

LLIB_HEADER_END